﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gits.domain.seguridad.entities
{
    public partial class Persona
    {
        public string NombreCompleto
        {
            get { return this.Nombre + " " + this.ApellidoPaterno + " " + this.ApellidoMaterno; }
        }
    }
}

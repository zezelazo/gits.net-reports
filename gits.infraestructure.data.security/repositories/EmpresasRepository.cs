﻿
using gits.domain.seguridad.da.context;
using gits.domain.seguridad.da.repositories;
using gits.domain.seguridad.entities;
using gits.infraestructure.data.core.repositories;

namespace gits.infraestructure.data.security.repositories
{
    public class EmpresasRepository : ReadOnlyRepository<Empresa>, IEmpresasRepository
    {
        public EmpresasRepository(ISeguridadContext prmContext) : base(prmContext)
        {
        }
    }
}
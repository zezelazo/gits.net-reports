﻿using gits.domain.seguridad.da.context;
using gits.domain.seguridad.da.repositories;
using gits.domain.seguridad.entities;
using gits.infraestructure.data.core.repositories;

namespace gits.infraestructure.data.security.repositories
{
    public class GruposRepository : ReadOnlyRepository<Grupo>, IGruposRepository
    {
        public GruposRepository(ISeguridadContext prmContext)
            : base(prmContext)
        {
        }
    }
}
﻿using gits.domain.facturacion.da.context;
using gits.domain.facturacion.da.repositories;
using gits.domain.facturacion.entities;
using gits.infraestructure.data.core.repositories;

namespace gits.infraestructure.data.facturacion.repositories
{
    public class TipoPagoAgenciamientoRepository : ReadOnlyRepository<TipoPagoAgenciamiento>, ITipoPagoAgenciamientoRepository 
    {
        public TipoPagoAgenciamientoRepository(IFacturacionContext prmContext)
            : base(prmContext)
        {
        }
    }
}
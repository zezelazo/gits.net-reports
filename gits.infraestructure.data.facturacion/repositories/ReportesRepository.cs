﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using gits.domain.facturacion.da.context;
using gits.domain.facturacion.da.repositories;
using gits.domain.facturacion.entities;
using gits.infraestructure.data.core.repositories;
using gits.infraestructure.data.facturacion.context;

namespace gits.infraestructure.data.facturacion.repositories
{
    public class ReportesRepository : RepositoryBase,IReportesRepository
    {
        public ReportesRepository(IFacturacionContext prmContext) : base(prmContext)
        {
        }

        private IFacturacionContext MyContext{
            get { return Context as facturacionContext; }
        }

        public IList<ListadoFacturaDetallado> GetListadoFacturaDetalladoSoloDetalles(string empresa, string desde, string hasta)
        {
            return MyContext.ListadoFacturasDetallado("I", empresa, "", "", "", "", "", "", "", "", desde, hasta).ToList();
        }

        public IList<ListadoFacturaDetallado> GetListadoFacturaDetallado(string empresa, string desde, string hasta)
        {
            return MyContext.ListadoFacturasDetallado("Z",empresa, "", "", "", "", "", "", "", "", desde, hasta).ToList();
        }

        public IList<ListadoFacturaConsolidado> GetListadoFacturaConsolidado(string empresa, string desde, string hasta)
        {
            return MyContext.ListadoFacturasConsolidado("D", empresa, "", "", "", "", "", "", "", "", desde, hasta).ToList();
        }

        public IList<FacturaAgenciamiento> GetListadoFacturaAgenciamiento(string empresa, string desde, string hasta, string invoice, string tipocobro, string cheque, string cliente)
        {
            return MyContext.FacturasAgenciamiento("L", empresa, invoice + "%", tipocobro, cheque + "%", cliente + "%", "%", desde, hasta).ToList();
        }

        public IList<FacturaAgenciamientoSobreestadia> GetListadoFacturaSobreestadia(string empresa, string desde, string hasta, string invoice, string tipocobro, string cheque, string cliente)
        {
            return MyContext.FacturasAgenciamientoSobreestadia("S", empresa, invoice + "%", tipocobro, cheque + "%", cliente + "%", "%", desde, hasta).ToList();
        } 
        
       

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Xml;
using gits.application.facturacion.reports;
using gits.distributedservices.facturacion.contracts;
using gits.distributedservices.facturacion.dto;
using gits.domain.facturacion.app;
using gits.domain.facturacion.da.context;
using gits.domain.facturacion.da.repositories;
using gits.infraestructure.data.facturacion.context;
using gits.infraestructure.data.facturacion.repositories;

namespace gits.distributedservices.facturacion.service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class FacturacionService : IFacturacionService
    {

        private IReportesRepository _repRepo;
        private ITipoPagoAgenciamientoRepository _tpaREpo;

        private IFacturacionContext _ctx;

        private IReportesFacturacionService _appSrv;
        public FacturacionService()
        {
            //TODO: IMPLEMENT PROPER FACTORY OR UNITY
            _ctx = new facturacionContext();
            _repRepo = new ReportesRepository(_ctx);
            _tpaREpo = new TipoPagoAgenciamientoRepository(_ctx);
            _appSrv = new ReportesFacturacionService(_repRepo, _tpaREpo);
        }
        public IEnumerable<TipoDePagoAgenciamiento> GetTiposDePagoAgenciamiento(string empresa)
        {
            return
                _appSrv.GetTipoPagoAgenciamiento(empresa).Select(
                    t => new TipoDePagoAgenciamiento() { Descripcion = t.Descripcion, Id = t.Id });
        }

        public Stream GetListadoFacturaDetalladoSoloDetalle(string empresa, DateTime desde, DateTime hasta)
        {
            Stream stream = new MemoryStream();
            try
            {
                var rpt = _appSrv.GetListadoFacturaDetalladoSoloDetalle(empresa, desde, hasta).Select(r => new FacturaDetallada()
                {
                    Agente = r.CLIENT_C_RAZON_SOCIAL,
                    Banco = r.BANCO_C_CODIGO_PAGO,
                    BL = r.DOCORI_C_NUMERO,
                    BL_M = r.DOCORI_C_NUMERO_MADRE,
                    Comprobante = r.TIPDOC_C_DESCRIPCION,
                    DescripcionDelServicio = r.SERVIC_C_DESCRIPCION,
                    Empresa = r.EMPRES_C_DESCRIPCION,
                    FacturaMontoBruto = r.FACTUR_N_MONTO_BRUTO,
                    FacturaMontoBrutoDolares = r.FACTUR_N_MONTO_BRUTO_DOLARES,
                    FacturaMontoImpuesto = r.FACTUR_N_IMPUESTO_VENTA,
                    FacturaMontoImpuestoDolares = r.FACTUR_N_IMPUESTO_VENTA_DOLARES,
                    FacturarA = r.CLIENT_C_RAZON_SOCIAL_FA,
                    Ruc = r.CLIENT_C_DOCUMENTO,
                    RucAgente = r.CLIENT_C_CODIGO,
                    Retencion = r.FACTUR_N_RETENCION,
                    Serie = r.SERIE_C_CODIGO,
                    Numero = r.FACTUR_C_NUMERO,
                    FechaPago = r.FACTUR_D_FECHA_PAGO,
                    NroDocumentoDePago = r.FACTUR_C_NUMERO_DOCUMENTO_PAGO,
                    FechaRegistro = r.FACTUR_D_FECHA,
                    FacturaMontoNeto = r.FACTUR_N_MONTO_NETO,
                    FacturaMontoNetoDolares = r.FACTUR_N_MONTO_NETO_DOLARES,
                    Moneda = r.MONEDA_C_DESCRIPCION,
                    SimboloMoneda = r.MONEDA_C_SIMBOLO,
                    Nave = r.NAVE_C_DESCRIPCION,
                    Viaje = r.NAVIRU_C_VIAJE,
                    TipoDeCambio = r.FACTUR_N_TIPO_CAMBIO,
                    Monto = r.FACTUR_N_MONTO_PAGO,
                    FormaDePago = r.FORPAG_C_DESCRIPCION
                }); 
                DataContractSerializer serializer = new DataContractSerializer(typeof(IEnumerable<FacturaDetallada>));
                XmlDictionaryWriter binaryDictionaryWriter = XmlDictionaryWriter.CreateBinaryWriter(stream);
                serializer.WriteObject(binaryDictionaryWriter, rpt); binaryDictionaryWriter.Flush();
            }
            catch (Exception ex)
            {
                string timestamp;
                //ExceptionHelper.HandleExceptionWrapper(ex, "Log Only", out timestamp);
            }
            stream.Position = 0;
            return stream;
        }

        public Stream GetListadoFacturaDetallado(string empresa, DateTime desde, DateTime hasta)
        {
            Stream stream = new MemoryStream();
            try
            {
                var rpt = _appSrv.GetListadoFacturaDetallado(empresa, desde, hasta).Select(r => new FacturaDetallada()
                                                                                                 {
                                                                                                     Agente = r.CLIENT_C_RAZON_SOCIAL,
                                                                                                     Banco = r.BANCO_C_CODIGO_PAGO,
                                                                                                     BL = r.DOCORI_C_NUMERO,
                                                                                                     BL_M = r.DOCORI_C_NUMERO_MADRE,
                                                                                                     Comprobante = r.TIPDOC_C_DESCRIPCION,
                                                                                                     DescripcionDelServicio = r.SERVIC_C_DESCRIPCION,
                                                                                                     Empresa = r.EMPRES_C_DESCRIPCION,
                                                                                                     FacturaMontoBruto = r.FACTUR_N_MONTO_BRUTO,
                                                                                                     FacturaMontoBrutoDolares = r.FACTUR_N_MONTO_BRUTO_DOLARES,
                                                                                                     FacturaMontoImpuesto = r.FACTUR_N_IMPUESTO_VENTA,
                                                                                                     FacturaMontoImpuestoDolares = r.FACTUR_N_IMPUESTO_VENTA_DOLARES,
                                                                                                     FacturarA = r.CLIENT_C_RAZON_SOCIAL_FA,
                                                                                                     Ruc = r.CLIENT_C_DOCUMENTO,
                                                                                                     RucAgente = r.CLIENT_C_CODIGO,
                                                                                                     Retencion = r.FACTUR_N_RETENCION,
                                                                                                     Serie = r.SERIE_C_CODIGO,
                                                                                                     Numero = r.FACTUR_C_NUMERO,
                                                                                                     FechaPago = r.FACTUR_D_FECHA_PAGO,
                                                                                                     NroDocumentoDePago = r.FACTUR_C_NUMERO_DOCUMENTO_PAGO,
                                                                                                     FechaRegistro = r.FACTUR_D_FECHA,
                                                                                                     FacturaMontoNeto = r.FACTUR_N_MONTO_NETO,
                                                                                                     FacturaMontoNetoDolares = r.FACTUR_N_MONTO_NETO_DOLARES,
                                                                                                     Moneda = r.MONEDA_C_DESCRIPCION,
                                                                                                     SimboloMoneda = r.MONEDA_C_SIMBOLO,
                                                                                                     Nave = r.NAVE_C_DESCRIPCION,
                                                                                                     Viaje = r.NAVIRU_C_VIAJE,
                                                                                                     TipoDeCambio = r.FACTUR_N_TIPO_CAMBIO,
                                                                                                     Monto = r.FACTUR_N_MONTO_PAGO,
                                                                                                     FormaDePago = r.FORPAG_C_DESCRIPCION
                                                                                                 });




                DataContractSerializer serializer = new DataContractSerializer(typeof(IEnumerable<FacturaDetallada>));
                XmlDictionaryWriter binaryDictionaryWriter = XmlDictionaryWriter.CreateBinaryWriter(stream);
                serializer.WriteObject(binaryDictionaryWriter, rpt); binaryDictionaryWriter.Flush();
            }
            catch (Exception ex)
            {
                string timestamp;
                //ExceptionHelper.HandleExceptionWrapper(ex, "Log Only", out timestamp);
            }
            stream.Position = 0;
            return stream;
        }

        public Stream GetListadoFacturaConsolidado(string empresa, DateTime desde, DateTime hasta)
        {
            Stream stream = new MemoryStream();
            try
            {
                var rpt = _appSrv.GetListadoFacturaConsolidado(empresa, desde, hasta).Select(r => new FacturaConsolidada()
                                                                                                   {
                                                                                                       Agente = r.CLIENT_C_RAZON_SOCIAL,
                                                                                                       Banco = r.BANCO_C_CODIGO_PAGO,
                                                                                                       BL = r.DOCORI_C_NUMERO,
                                                                                                       BL_M = r.DOCORI_C_NUMERO_MADRE,
                                                                                                       Comprobante = r.TIPDOC_C_DESCRIPCION,
                                                                                                       Empresa = r.EMPRES_C_DESCRIPCION,
                                                                                                       FacturaMontoBruto = r.FACTUR_N_MONTO_BRUTO,
                                                                                                       FacturaMontoBrutoDolares = r.FACTUR_N_MONTO_BRUTO_DOLARES,
                                                                                                       FacturaMontoImpuesto = r.FACTUR_N_IMPUESTO_VENTA,
                                                                                                       FacturaMontoImpuestoDolares = r.FACTUR_N_IMPUESTO_VENTA_DOLARES,
                                                                                                       FacturarA = r.CLIENT_C_RAZON_SOCIAL_FA,
                                                                                                       Ruc = r.CLIENT_C_DOCUMENTO,
                                                                                                       RucAgente = r.CLIENT_C_CODIGO,
                                                                                                       Retencion = r.FACTUR_N_RETENCION,
                                                                                                       Serie = r.SERIE_C_CODIGO,
                                                                                                       Numero = r.FACTUR_C_NUMERO,
                                                                                                       FechaPago = r.FACTUR_D_FECHA_PAGO,
                                                                                                       NroDocumentoDePago = r.FACTUR_C_NUMERO_DOCUMENTO_PAGO,
                                                                                                       FechaRegistro = r.FACTUR_D_FECHA,
                                                                                                       FacturaMontoNeto = r.FACTUR_N_MONTO_NETO,
                                                                                                       FacturaMontoNetoDolares = r.FACTUR_N_MONTO_NETO_DOLARES,
                                                                                                       Moneda = r.MONEDA_C_DESCRIPCION,
                                                                                                       SimboloMoneda = r.MONEDA_C_SIMBOLO,
                                                                                                       Nave = r.NAVE_C_DESCRIPCION,
                                                                                                       Viaje = r.NAVIRU_C_VIAJE,
                                                                                                       TipoDeCambio = r.FACTUR_N_TIPO_CAMBIO,
                                                                                                       Monto = r.FACTUR_N_MONTO_PAGO,
                                                                                                       FormaDePago = r.FORPAG_C_DESCRIPCION
                                                                                                   });
                DataContractSerializer serializer = new DataContractSerializer(typeof(IEnumerable<FacturaConsolidada>));
                XmlDictionaryWriter binaryDictionaryWriter = XmlDictionaryWriter.CreateBinaryWriter(stream);
                serializer.WriteObject(binaryDictionaryWriter, rpt); binaryDictionaryWriter.Flush();
            }
            catch (Exception ex)
            {
                string timestamp;
                //ExceptionHelper.HandleExceptionWrapper(ex, "Log Only", out timestamp);
            }
            stream.Position = 0;
            return stream;
        }

        public Stream GetListadoFacturaAgenciamiento(string empresa, DateTime desde, DateTime hasta, string invoice, string tipocobro, string cheque, string cliente)
        {
            Stream stream = new MemoryStream();
            try
            {
                var rpt = _appSrv.GetListadoFacturaAgenciamiento(empresa,
                                                              desde,
                                                              hasta,
                                                              invoice,
                                                              tipocobro,
                                                              cheque,
                                                              cliente)
                                                            .Select(r => new FacturaAgenciamiento()
                                                                       {
                                                                           Empresa = r.EMPRES_C_CODIGO,
                                                                           Invoice = r.FACAGE_C_NUMERO,
                                                                           Cheque = r.FACAGE_C_NUMERO_CHEQUE,
                                                                           TipoDeCobro = r.TIPAAG_C_DESCRIPCION,
                                                                           Fecha = r.FACAGE_D_FECHA,
                                                                           BL = r.FACAGE_C_DOCUMENTO_ORIGEN,
                                                                           Banco = r.BANCO_C_DESCRIPCION,
                                                                           MontoNeto = r.FACAGE_N_NETO,
                                                                           Estado = r.FACAGE_C_SITUACION
                                                                       });
                DataContractSerializer serializer = new DataContractSerializer(typeof(IEnumerable<FacturaAgenciamiento>));
                XmlDictionaryWriter binaryDictionaryWriter = XmlDictionaryWriter.CreateBinaryWriter(stream);
                serializer.WriteObject(binaryDictionaryWriter, rpt); binaryDictionaryWriter.Flush();
            }
            catch (Exception ex)
            {
                string timestamp;
                //ExceptionHelper.HandleExceptionWrapper(ex, "Log Only", out timestamp);
            }
            stream.Position = 0;
            return stream;
        }

        public Stream GetListadoFacturaSobreestadia(string empresa, DateTime desde, DateTime hasta, string invoice, string tipocobro, string cheque, string cliente)
        {
            Stream stream = new MemoryStream();
            try
            {
                var rpt = _appSrv.GetListadoFacturaSobreestadia(empresa,
                                                             desde,
                                                             hasta,
                                                             invoice,
                                                             tipocobro,
                                                             cheque,
                                                             cliente)
                                                            .Select(r => new FacturaAgenciamientoSobreestadia()
                                                                             {
                                                                                 Invoice = r.FACAGE_C_NUMERO,
                                                                                 Cheque = r.FACAGE_C_NUMERO_CHEQUE,
                                                                                 TipoDeCobro = r.TIPAAG_C_DESCRIPCION,
                                                                                 Fecha = r.FACAGE_D_FECHA,
                                                                                 BL = r.FACAGE_C_DOCUMENTO_ORIGEN,
                                                                                 Banco = r.BANCO_C_DESCRIPCION,
                                                                                 Estado = r.FACAGE_C_SITUACION,
                                                                                 Ruc = r.CLIENT_C_CODIGO,
                                                                                 Cliente = r.CLIENT_C_RAZON_SOCIAL,
                                                                                 Total = r.FACAGE_N_TOTAL,
                                                                                 Retencion = r.FACAGE_N_RETENCION,
                                                                                 Neto = r.FACAGE_N_NETO,
                                                                                 DesdeHasta = r.DESDEHASTA,
                                                                                 TotalCheque = r.FACAGE_N_TOTAL_CHEQUE
                                                                             });
                DataContractSerializer serializer = new DataContractSerializer(typeof(IEnumerable<FacturaAgenciamientoSobreestadia>));
                XmlDictionaryWriter binaryDictionaryWriter = XmlDictionaryWriter.CreateBinaryWriter(stream);
                serializer.WriteObject(binaryDictionaryWriter, rpt); binaryDictionaryWriter.Flush();
            }
            catch (Exception ex)
            {
                string timestamp;
                //ExceptionHelper.HandleExceptionWrapper(ex, "Log Only", out timestamp);
            }
            stream.Position = 0;
            return stream;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel; 
using gits.distributedservices.facturacion.dto; 
using FacturaAgenciamiento = gits.distributedservices.facturacion.dto.FacturaAgenciamiento;
using FacturaAgenciamientoSobreestadia = gits.distributedservices.facturacion.dto.FacturaAgenciamientoSobreestadia;

namespace gits.distributedservices.facturacion.contracts
{
    [ServiceContract]
    public interface IFacturacionService
    {
        [OperationContract]
        IEnumerable<TipoDePagoAgenciamiento> GetTiposDePagoAgenciamiento(string  empresa);
        [OperationContract]
        Stream GetListadoFacturaDetalladoSoloDetalle(string empresa, DateTime desde, DateTime hasta);
        [OperationContract]
        Stream GetListadoFacturaDetallado(string empresa, DateTime desde, DateTime hasta);
        [OperationContract]
        Stream GetListadoFacturaConsolidado(string empresa, DateTime desde, DateTime hasta);
        [OperationContract]
        Stream GetListadoFacturaAgenciamiento(string empresa, DateTime desde, DateTime hasta, string invoice, string tipocobro, string cheque, string cliente);
        [OperationContract]
        Stream GetListadoFacturaSobreestadia(string empresa, DateTime desde, DateTime hasta, string invoice, string tipocobro, string cheque, string cliente);
    }
}

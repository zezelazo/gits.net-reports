﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace gits.domain.core.entities.ef4
{
    [CollectionDataContract(Name = "ObjectsRemovedFromCollectionProperties",
        ItemName = "DeletedObjectsForProperty", KeyName = "CollectionPropertyName", ValueName = "DeletedObjects")]
    public class ObjectsRemovedFromCollectionProperties : Dictionary<string, ObjectList> { }
}
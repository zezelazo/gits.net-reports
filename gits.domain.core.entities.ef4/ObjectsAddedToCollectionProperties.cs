﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace gits.domain.core.entities.ef4
{
   
  

    [CollectionDataContract(Name = "ObjectsAddedToCollectionProperties",
                            ItemName = "AddedObjectsForProperty", 
                            KeyName = "CollectionPropertyName", 
                            ValueName = "AddedObjects")]
    public class ObjectsAddedToCollectionProperties : Dictionary<string, ObjectList> { }
}

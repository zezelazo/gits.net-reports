﻿using System;

namespace gits.domain.core.entities.ef4
{
    public class ObjectStateChangingEventArgs : EventArgs
    {
        public ObjectState NewState { get; set; }
    }
}
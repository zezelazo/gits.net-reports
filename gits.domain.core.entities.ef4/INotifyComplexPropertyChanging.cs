﻿using System;

namespace gits.domain.core.entities.ef4
{
    public interface INotifyComplexPropertyChanging
    {
        event EventHandler ComplexPropertyChanging;
    }
}
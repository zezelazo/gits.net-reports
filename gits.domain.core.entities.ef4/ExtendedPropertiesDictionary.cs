﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace gits.domain.core.entities.ef4
{
    [CollectionDataContract(Name = "ExtendedPropertiesDictionary",
        ItemName = "ExtendedProperties", KeyName = "Name", ValueName = "ExtendedProperty")]
    public class ExtendedPropertiesDictionary : Dictionary<string, object> { }
}
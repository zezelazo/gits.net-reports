﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace gits.domain.core.entities.ef4
{
    [CollectionDataContract(Name = "OriginalValuesDictionary",
        ItemName = "OriginalValues", KeyName = "Name", ValueName = "OriginalValue")]
    public class OriginalValuesDictionary : Dictionary<string, object> { }
}
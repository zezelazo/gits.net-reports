﻿namespace gits.domain.core.entities.ef4
{
    public interface IObjectWithChangeTracker
    {
        ObjectChangeTracker ChangeTracker { get; }
    }
}
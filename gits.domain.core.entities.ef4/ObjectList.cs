﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace gits.domain.core.entities.ef4
{
    [CollectionDataContract(ItemName = "ObjectValue")]
    public class ObjectList : List<object> { }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using gits.domain.seguridad.app;
using gits.domain.seguridad.da.repositories;
using gits.domain.seguridad.entities;
using Microsoft.VisualBasic;

namespace gits.application.seguridad.login
{
    public class LoginService : ILoginService,IDisposable
    {
        private IUsuariosRepository repoUsuarios;
        private IEmpresasRepository repoEmpresas;
        private IGruposRepository repoGrupos;

        public LoginService(IUsuariosRepository prmRepoUsuarios,
                            IEmpresasRepository prmRepoEmpresas, 
                            IGruposRepository prmRepoGrupos)
        {
            repoUsuarios = prmRepoUsuarios;
            repoEmpresas = prmRepoEmpresas;
            repoGrupos = prmRepoGrupos;
        }

        public IList<Empresa> GetEmpresas()
        {
            return repoEmpresas.Query().OrderBy(e=>e.Descripcion).ToList();
        }

        public bool CheckIfUserExist(string prmUserName)
        {
            return repoUsuarios.Find(u => u.Id.ToUpper().Trim() == prmUserName.ToUpper().Trim()).Any();
        }

        public bool CheckIfUserExistInEmpresa(string prmUserName, string prmEmpresaCode)
        {
            var empUsr = this.GetUserEmpresas(prmUserName).Select(e=>e.Id.Trim().ToUpper());
            return empUsr.Contains(prmEmpresaCode.ToUpper().Trim());
        }

        public bool CheckIfIsUserPassword(string prmUserName, string prmUserPassword)
        {
            var usr = GetUserData(prmUserName);
            var pass = EncriptPassword(prmUserPassword);
            return usr.Clave.ToUpper().Trim() == pass.ToUpper().Trim();
        }

        public Usuario GetUserData(string prmUserCode)
        {
            return repoUsuarios.Find(u => u.Id.ToUpper().Trim() == prmUserCode.ToUpper().Trim())
                               .FirstOrDefault();
        }

        public Grupo GetUserGroup(string prmUserCode)
        {
            return repoUsuarios.Find(u => u.Id.ToUpper().Trim() == prmUserCode.ToUpper().Trim())
                               .Select(u=>u.Grupo)
                               .FirstOrDefault();
        }

        public Persona GetUserPersonalInfo(string prmUserCode)
        {
            return repoUsuarios.Find(u => u.Id.ToUpper().Trim() == prmUserCode.ToUpper().Trim())
                                .Select(u=>u.Persona)
                               .FirstOrDefault();
        }

        public IList<Empresa> GetUserEmpresas(string prmUserCode)
        {
            return repoUsuarios.Find(u => u.Id.ToUpper().Trim() == prmUserCode.ToUpper().Trim()) 
                                    .Select(u => u.Empresas.Select(ue => ue.Empresa))
                                    .FirstOrDefault()
                                    .ToList();
        }

        public Usuario GetFullUserData(string prmUserCode)
        {
            var usr = GetUserData(prmUserCode);
            if (usr.Grupo == null) usr.Grupo = GetUserGroup(prmUserCode);
            if (usr.Persona == null) usr.Persona = GetUserPersonalInfo(prmUserCode);
            return usr;
        }

        private string EncriptPassword(string prmPassword)
        {
            if (prmPassword.Trim() == "") return "";
            const string encripta = "0501030904020807050103090402080705010309";
            var cadena = "";
            var a = 1;
            for (var j = 1; j <= prmPassword.Length; j++)
            {
                if (a < encripta.Length)
                {
                    cadena = cadena + Strings.Chr(Strings.Asc(Strings.Mid(prmPassword, j, 1)) + Convert.ToInt32(Strings.Mid(encripta, a, 2)));
                }
                a += 2;
            }
            return cadena.ToUpper().Trim();
        }

        #region Implementation of IDisposable

        public void Dispose()
        {
            repoUsuarios.Dispose();
            repoEmpresas.Dispose();
            repoGrupos.Dispose();
        }

        #endregion
    }
}

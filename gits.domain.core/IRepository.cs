﻿namespace gits.domain.core
{
    public interface IRepository<T>:IReadOnlyRepository<T> where T : class
    {
        void Add(T entity);
        void Remove(T entity);
        void Update(T current);
        void Attach(T current);
        void SubmitChanges();
    }
}
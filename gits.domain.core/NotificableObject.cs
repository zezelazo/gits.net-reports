﻿using System.ComponentModel;

namespace gits.domain.core
{
    public class NotificableObject : INotifyPropertyChanged
    {
        public void  RaisePropertyChanged(string prmPropertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(prmPropertyName));        
            }
        }

        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add
            {
                _propertyChanged += value;
            }
            remove
            {
                _propertyChanged -= value;
            }
        }
        private event PropertyChangedEventHandler _propertyChanged;
    }
}

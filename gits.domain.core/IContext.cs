﻿using System;

namespace gits.domain.core
{
    public interface IContext : IDisposable
    {
        int SaveChanges();
    }
}

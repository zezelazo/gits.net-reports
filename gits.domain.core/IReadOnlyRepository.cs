﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace gits.domain.core
{
    public interface IReadOnlyRepository<T>:IDisposable where T:class 
    {
        IQueryable<T> Query();
        
        IQueryable<T> Find(Expression<Func<T, bool>> expression);
    }
}
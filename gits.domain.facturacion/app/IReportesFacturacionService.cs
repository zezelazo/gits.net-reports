﻿using System;
using System.Collections.Generic;
using gits.domain.facturacion.entities; 

namespace gits.domain.facturacion.app
{
    public interface IReportesFacturacionService :IDisposable
    {
        IList<ListadoFacturaDetallado> GetListadoFacturaDetalladoSoloDetalle(string empresa, DateTime desde, DateTime hasta);
        IList<ListadoFacturaDetallado> GetListadoFacturaDetallado(string empresa, DateTime desde, DateTime hasta);
        IList<ListadoFacturaConsolidado> GetListadoFacturaConsolidado(string empresa, DateTime desde, DateTime hasta);
        IList<FacturaAgenciamiento> GetListadoFacturaAgenciamiento(string empresa, DateTime desde, DateTime hasta, string invoice, string tipocobro, string cheque, string cliente);
        IList<FacturaAgenciamientoSobreestadia> GetListadoFacturaSobreestadia(string empresa, DateTime desde, DateTime hasta, string invoice, string tipocobro, string cheque, string cliente);

        IList<TipoPagoAgenciamiento> GetTipoPagoAgenciamiento(string empresa);
    }
}

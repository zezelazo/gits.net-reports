﻿using gits.domain.core;
using gits.domain.facturacion.entities;

namespace gits.domain.facturacion.da.repositories
{
    public interface  ITipoPagoAgenciamientoRepository : IReadOnlyRepository<TipoPagoAgenciamiento>
    {
        
    }
}
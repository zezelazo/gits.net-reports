﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using gits.domain.facturacion.entities;

namespace gits.domain.facturacion.da.repositories
{
    public interface IReportesRepository : IDisposable
    {
        IList<ListadoFacturaDetallado> GetListadoFacturaDetalladoSoloDetalles(string empresa, string desde, string hasta);
        IList<ListadoFacturaDetallado> GetListadoFacturaDetallado(string empresa,string desde,string hasta);
        IList<ListadoFacturaConsolidado> GetListadoFacturaConsolidado(string empresa, string desde, string hasta);
        IList<FacturaAgenciamiento> GetListadoFacturaAgenciamiento(string empresa, string desde, string hasta, string invoice, string tipocobro, string cheque, string cliente);
        IList<FacturaAgenciamientoSobreestadia> GetListadoFacturaSobreestadia(string empresa, string desde, string hasta, string invoice, string tipocobro, string cheque, string cliente);
    }
}

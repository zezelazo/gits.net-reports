﻿#region

using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

#endregion

namespace gits.presentation.sl4.app.Converters
{
    public class StateToVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (System.Convert.ToInt32(value) == System.Convert.ToInt32(parameter))
            {
                return Visibility.Visible;
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
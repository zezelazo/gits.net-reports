﻿#region

using System.IO;
using System.Windows.Controls;
using gits.presentation.sl4.app.ViewModels;

#endregion

namespace gits.presentation.sl4.app.Views
{
    public partial class ListadoFacturaClienteV : Page
    {
        public ListadoFacturaClienteV()
        {
            InitializeComponent();
        }

        private void Exportar(object sender, System.Windows.RoutedEventArgs e)
        {
            var sfd = new SaveFileDialog
            {
                Filter = "Archivo de Microsoft Excel(*.xls)|*.xls"
            };

            if (sfd.ShowDialog() == true)
            {
                Stream stream = sfd.OpenFile();
                ((ListadoFacturaClienteVM)this.DataContext).Exportar.Execute(stream);

            }
        }
         
    }
}
﻿#region

using System.IO;
using System.Windows.Controls;
using System.Windows.Navigation; 
using gits.presentation.sl4.app.ViewModels;

#endregion

namespace gits.presentation.sl4.app.Views
{
    public partial class ListadoFacturaAgenciamientoV : Page
    {
        public ListadoFacturaAgenciamientoV()
        {
            InitializeComponent();
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        } 

        private void Exportar(object sender, System.Windows.RoutedEventArgs e)
        {
            var sfd = new SaveFileDialog
            {
                Filter = "Archivo de Microsoft Excel(*.xls)|*.xls"
            };

            if (sfd.ShowDialog() == true)
            {
                Stream stream = sfd.OpenFile();
                ((ListadoFacturaAgenciamientoVM)this.DataContext).Exportar.CanExecute(stream);
            }

        }
    }
}
﻿#region

using System;
using System.CodeDom.Compiler;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading;

#endregion

namespace gits.presentation.sl4.app.svcSeguridad
{
    [DebuggerStepThrough]
    [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
    [DataContract(Name = "UserCredentials",
        Namespace = "http://schemas.datacontract.org/2004/07/gits.distributedservices.seguridad.dto")]
    public class UserCredentials : object, INotifyPropertyChanged
    {
        private string ClientIpField;

        private string ClientNameField;

        private ClientsTypesEnum ClientTypeField;

        private string UserCompanyField;

        private string UserNameField;

        private string UserPasswordField;

        [DataMember]
        public string ClientIp
        {
            get { return ClientIpField; }
            set
            {
                if ((ReferenceEquals(ClientIpField, value) != true))
                {
                    ClientIpField = value;
                    RaisePropertyChanged("ClientIp");
                }
            }
        }

        [DataMember]
        public string ClientName
        {
            get { return ClientNameField; }
            set
            {
                if ((ReferenceEquals(ClientNameField, value) != true))
                {
                    ClientNameField = value;
                    RaisePropertyChanged("ClientName");
                }
            }
        }

        [DataMember]
        public ClientsTypesEnum ClientType
        {
            get { return ClientTypeField; }
            set
            {
                if ((ClientTypeField.Equals(value) != true))
                {
                    ClientTypeField = value;
                    RaisePropertyChanged("ClientType");
                }
            }
        }

        [DataMember]
        public string UserCompany
        {
            get { return UserCompanyField; }
            set
            {
                if ((ReferenceEquals(UserCompanyField, value) != true))
                {
                    UserCompanyField = value;
                    RaisePropertyChanged("UserCompany");
                }
            }
        }

        [DataMember]
        public string UserName
        {
            get { return UserNameField; }
            set
            {
                if ((ReferenceEquals(UserNameField, value) != true))
                {
                    UserNameField = value;
                    RaisePropertyChanged("UserName");
                }
            }
        }

        [DataMember]
        public string UserPassword
        {
            get { return UserPasswordField; }
            set
            {
                if ((ReferenceEquals(UserPasswordField, value) != true))
                {
                    UserPasswordField = value;
                    RaisePropertyChanged("UserPassword");
                }
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
    [DataContract(Name = "ClientsTypesEnum",
        Namespace = "http://schemas.datacontract.org/2004/07/gits.distributedservices.seguridad.dto")]
    public enum ClientsTypesEnum
    {
        [EnumMember] Desktop = 0,

        [EnumMember] Web = 1,

        [EnumMember] Silverlight = 2,

        [EnumMember] WMobile = 3,
    }

    [DebuggerStepThrough]
    [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
    [DataContract(Name = "UserData",
        Namespace = "http://schemas.datacontract.org/2004/07/gits.distributedservices.seguridad.dto")]
    public class UserData : object, INotifyPropertyChanged
    {
        private LoginError LoginErrorField;

        private Guid SessionCodeField;

        private string UserCodeField;

        private string UserEmpresaField;

        private string UserEmpresaIdField;

        private string UserNameField;

        private string UserRealNameField;

        [DataMember]
        public LoginError LoginError
        {
            get { return LoginErrorField; }
            set
            {
                if ((ReferenceEquals(LoginErrorField, value) != true))
                {
                    LoginErrorField = value;
                    RaisePropertyChanged("LoginError");
                }
            }
        }

        [DataMember]
        public Guid SessionCode
        {
            get { return SessionCodeField; }
            set
            {
                if ((SessionCodeField.Equals(value) != true))
                {
                    SessionCodeField = value;
                    RaisePropertyChanged("SessionCode");
                }
            }
        }

        [DataMember]
        public string UserCode
        {
            get { return UserCodeField; }
            set
            {
                if ((ReferenceEquals(UserCodeField, value) != true))
                {
                    UserCodeField = value;
                    RaisePropertyChanged("UserCode");
                }
            }
        }

        [DataMember]
        public string UserEmpresa
        {
            get { return UserEmpresaField; }
            set
            {
                if ((ReferenceEquals(UserEmpresaField, value) != true))
                {
                    UserEmpresaField = value;
                    RaisePropertyChanged("UserEmpresa");
                }
            }
        }

        [DataMember]
        public string UserEmpresaId
        {
            get { return UserEmpresaIdField; }
            set
            {
                if ((ReferenceEquals(UserEmpresaIdField, value) != true))
                {
                    UserEmpresaIdField = value;
                    RaisePropertyChanged("UserEmpresaId");
                }
            }
        }

        [DataMember]
        public string UserName
        {
            get { return UserNameField; }
            set
            {
                if ((ReferenceEquals(UserNameField, value) != true))
                {
                    UserNameField = value;
                    RaisePropertyChanged("UserName");
                }
            }
        }

        [DataMember]
        public string UserRealName
        {
            get { return UserRealNameField; }
            set
            {
                if ((ReferenceEquals(UserRealNameField, value) != true))
                {
                    UserRealNameField = value;
                    RaisePropertyChanged("UserRealName");
                }
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [DebuggerStepThrough]
    [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
    [DataContract(Name = "LoginError",
        Namespace = "http://schemas.datacontract.org/2004/07/gits.distributedservices.seguridad.dto")]
    public class LoginError : object, INotifyPropertyChanged
    {
        private int ErrCodeField;

        private string ErrMsgField;

        [DataMember]
        public int ErrCode
        {
            get { return ErrCodeField; }
            set
            {
                if ((ErrCodeField.Equals(value) != true))
                {
                    ErrCodeField = value;
                    RaisePropertyChanged("ErrCode");
                }
            }
        }

        [DataMember]
        public string ErrMsg
        {
            get { return ErrMsgField; }
            set
            {
                if ((ReferenceEquals(ErrMsgField, value) != true))
                {
                    ErrMsgField = value;
                    RaisePropertyChanged("ErrMsg");
                }
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [DebuggerStepThrough]
    [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
    [DataContract(Name = "Empresa",
        Namespace = "http://schemas.datacontract.org/2004/07/gits.distributedservices.seguridad.dto")]
    public class Empresa : object, INotifyPropertyChanged
    {
        private string DescripcionField;

        private string IdField;

        [DataMember]
        public string Descripcion
        {
            get { return DescripcionField; }
            set
            {
                if ((ReferenceEquals(DescripcionField, value) != true))
                {
                    DescripcionField = value;
                    RaisePropertyChanged("Descripcion");
                }
            }
        }

        [DataMember]
        public string Id
        {
            get { return IdField; }
            set
            {
                if ((ReferenceEquals(IdField, value) != true))
                {
                    IdField = value;
                    RaisePropertyChanged("Id");
                }
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [GeneratedCode("System.ServiceModel", "4.0.0.0")]
    [ServiceContract(ConfigurationName = "svcSeguridad.ILoginService")]
    public interface ILoginService
    {
        [OperationContract(AsyncPattern = true, Action = "http://tempuri.org/ILoginService/Login",
            ReplyAction = "http://tempuri.org/ILoginService/LoginResponse")]
        IAsyncResult BeginLogin(UserCredentials prmCredentials, AsyncCallback callback, object asyncState);

        UserData EndLogin(IAsyncResult result);

        [OperationContract(AsyncPattern = true, Action = "http://tempuri.org/ILoginService/LogOut",
            ReplyAction = "http://tempuri.org/ILoginService/LogOutResponse")]
        IAsyncResult BeginLogOut(string prmSession, AsyncCallback callback, object asyncState);

        int EndLogOut(IAsyncResult result);

        [OperationContract(AsyncPattern = true, Action = "http://tempuri.org/ILoginService/IsValidSession",
            ReplyAction = "http://tempuri.org/ILoginService/IsValidSessionResponse")]
        IAsyncResult BeginIsValidSession(string prmSessionId, AsyncCallback callback, object asyncState);

        bool EndIsValidSession(IAsyncResult result);

        [OperationContract(AsyncPattern = true, Action = "http://tempuri.org/ILoginService/ListaEmpresas",
            ReplyAction = "http://tempuri.org/ILoginService/ListaEmpresasResponse")]
        IAsyncResult BeginListaEmpresas(AsyncCallback callback, object asyncState);

        ObservableCollection<Empresa> EndListaEmpresas(IAsyncResult result);
    }

    [GeneratedCode("System.ServiceModel", "4.0.0.0")]
    public interface ILoginServiceChannel : ILoginService, IClientChannel
    {
    }

    [DebuggerStepThrough]
    [GeneratedCode("System.ServiceModel", "4.0.0.0")]
    public class LoginCompletedEventArgs : AsyncCompletedEventArgs
    {
        private object[] results;

        public LoginCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        public UserData Result
        {
            get
            {
                base.RaiseExceptionIfNecessary();
                return ((UserData) (results[0]));
            }
        }
    }

    [DebuggerStepThrough]
    [GeneratedCode("System.ServiceModel", "4.0.0.0")]
    public class LogOutCompletedEventArgs : AsyncCompletedEventArgs
    {
        private object[] results;

        public LogOutCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        public int Result
        {
            get
            {
                base.RaiseExceptionIfNecessary();
                return ((int) (results[0]));
            }
        }
    }

    [DebuggerStepThrough]
    [GeneratedCode("System.ServiceModel", "4.0.0.0")]
    public class IsValidSessionCompletedEventArgs : AsyncCompletedEventArgs
    {
        private object[] results;

        public IsValidSessionCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        public bool Result
        {
            get
            {
                base.RaiseExceptionIfNecessary();
                return ((bool) (results[0]));
            }
        }
    }

    [DebuggerStepThrough]
    [GeneratedCode("System.ServiceModel", "4.0.0.0")]
    public class ListaEmpresasCompletedEventArgs : AsyncCompletedEventArgs
    {
        private object[] results;

        public ListaEmpresasCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        public ObservableCollection<Empresa> Result
        {
            get
            {
                base.RaiseExceptionIfNecessary();
                return ((ObservableCollection<Empresa>) (results[0]));
            }
        }
    }

    [DebuggerStepThrough]
    [GeneratedCode("System.ServiceModel", "4.0.0.0")]
    public class LoginServiceClient : ClientBase<ILoginService>, ILoginService
    {
        private BeginOperationDelegate onBeginCloseDelegate;
        private BeginOperationDelegate onBeginIsValidSessionDelegate;

        private BeginOperationDelegate onBeginListaEmpresasDelegate;
        private BeginOperationDelegate onBeginLogOutDelegate;
        private BeginOperationDelegate onBeginLoginDelegate;

        private BeginOperationDelegate onBeginOpenDelegate;
        private SendOrPostCallback onCloseCompletedDelegate;
        private EndOperationDelegate onEndCloseDelegate;
        private EndOperationDelegate onEndIsValidSessionDelegate;
        private EndOperationDelegate onEndListaEmpresasDelegate;
        private EndOperationDelegate onEndLogOutDelegate;
        private EndOperationDelegate onEndLoginDelegate;

        private EndOperationDelegate onEndOpenDelegate;
        private SendOrPostCallback onIsValidSessionCompletedDelegate;
        private SendOrPostCallback onListaEmpresasCompletedDelegate;
        private SendOrPostCallback onLogOutCompletedDelegate;
        private SendOrPostCallback onLoginCompletedDelegate;

        private SendOrPostCallback onOpenCompletedDelegate;

        public LoginServiceClient()
        {
        }

        public LoginServiceClient(string endpointConfigurationName) :
            base(endpointConfigurationName)
        {
        }

        public LoginServiceClient(string endpointConfigurationName, string remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public LoginServiceClient(string endpointConfigurationName, EndpointAddress remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public LoginServiceClient(Binding binding, EndpointAddress remoteAddress) :
            base(binding, remoteAddress)
        {
        }

        public CookieContainer CookieContainer
        {
            get
            {
                IHttpCookieContainerManager httpCookieContainerManager =
                    InnerChannel.GetProperty<IHttpCookieContainerManager>();
                if ((httpCookieContainerManager != null))
                {
                    return httpCookieContainerManager.CookieContainer;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                IHttpCookieContainerManager httpCookieContainerManager =
                    InnerChannel.GetProperty<IHttpCookieContainerManager>();
                if ((httpCookieContainerManager != null))
                {
                    httpCookieContainerManager.CookieContainer = value;
                }
                else
                {
                    throw new InvalidOperationException(
                        "Unable to set the CookieContainer. Please make sure the binding contains an HttpC" +
                        "ookieContainerBindingElement.");
                }
            }
        }

        #region ILoginService Members

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        IAsyncResult ILoginService.BeginLogin(UserCredentials prmCredentials, AsyncCallback callback, object asyncState)
        {
            return base.Channel.BeginLogin(prmCredentials, callback, asyncState);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        UserData ILoginService.EndLogin(IAsyncResult result)
        {
            return base.Channel.EndLogin(result);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        IAsyncResult ILoginService.BeginLogOut(string prmSession, AsyncCallback callback, object asyncState)
        {
            return base.Channel.BeginLogOut(prmSession, callback, asyncState);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        int ILoginService.EndLogOut(IAsyncResult result)
        {
            return base.Channel.EndLogOut(result);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        IAsyncResult ILoginService.BeginIsValidSession(string prmSessionId, AsyncCallback callback, object asyncState)
        {
            return base.Channel.BeginIsValidSession(prmSessionId, callback, asyncState);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        bool ILoginService.EndIsValidSession(IAsyncResult result)
        {
            return base.Channel.EndIsValidSession(result);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        IAsyncResult ILoginService.BeginListaEmpresas(AsyncCallback callback, object asyncState)
        {
            return base.Channel.BeginListaEmpresas(callback, asyncState);
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        ObservableCollection<Empresa> ILoginService.EndListaEmpresas(IAsyncResult result)
        {
            return base.Channel.EndListaEmpresas(result);
        }

        #endregion

        public event EventHandler<LoginCompletedEventArgs> LoginCompleted;

        public event EventHandler<LogOutCompletedEventArgs> LogOutCompleted;

        public event EventHandler<IsValidSessionCompletedEventArgs> IsValidSessionCompleted;

        public event EventHandler<ListaEmpresasCompletedEventArgs> ListaEmpresasCompleted;

        public event EventHandler<AsyncCompletedEventArgs> OpenCompleted;

        public event EventHandler<AsyncCompletedEventArgs> CloseCompleted;

        private IAsyncResult OnBeginLogin(object[] inValues, AsyncCallback callback, object asyncState)
        {
            UserCredentials prmCredentials = ((UserCredentials) (inValues[0]));
            return ((ILoginService) (this)).BeginLogin(prmCredentials, callback, asyncState);
        }

        private object[] OnEndLogin(IAsyncResult result)
        {
            UserData retVal = ((ILoginService) (this)).EndLogin(result);
            return new object[]
                       {
                           retVal
                       };
        }

        private void OnLoginCompleted(object state)
        {
            if ((LoginCompleted != null))
            {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs) (state));
                LoginCompleted(this, new LoginCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
            }
        }

        public void LoginAsync(UserCredentials prmCredentials)
        {
            LoginAsync(prmCredentials, null);
        }

        public void LoginAsync(UserCredentials prmCredentials, object userState)
        {
            if ((onBeginLoginDelegate == null))
            {
                onBeginLoginDelegate = new BeginOperationDelegate(OnBeginLogin);
            }
            if ((onEndLoginDelegate == null))
            {
                onEndLoginDelegate = new EndOperationDelegate(OnEndLogin);
            }
            if ((onLoginCompletedDelegate == null))
            {
                onLoginCompletedDelegate = new SendOrPostCallback(OnLoginCompleted);
            }
            base.InvokeAsync(onBeginLoginDelegate, new object[]
                                                       {
                                                           prmCredentials
                                                       }, onEndLoginDelegate, onLoginCompletedDelegate, userState);
        }

        private IAsyncResult OnBeginLogOut(object[] inValues, AsyncCallback callback, object asyncState)
        {
            string prmSession = ((string) (inValues[0]));
            return ((ILoginService) (this)).BeginLogOut(prmSession, callback, asyncState);
        }

        private object[] OnEndLogOut(IAsyncResult result)
        {
            int retVal = ((ILoginService) (this)).EndLogOut(result);
            return new object[]
                       {
                           retVal
                       };
        }

        private void OnLogOutCompleted(object state)
        {
            if ((LogOutCompleted != null))
            {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs) (state));
                LogOutCompleted(this, new LogOutCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
            }
        }

        public void LogOutAsync(string prmSession)
        {
            LogOutAsync(prmSession, null);
        }

        public void LogOutAsync(string prmSession, object userState)
        {
            if ((onBeginLogOutDelegate == null))
            {
                onBeginLogOutDelegate = new BeginOperationDelegate(OnBeginLogOut);
            }
            if ((onEndLogOutDelegate == null))
            {
                onEndLogOutDelegate = new EndOperationDelegate(OnEndLogOut);
            }
            if ((onLogOutCompletedDelegate == null))
            {
                onLogOutCompletedDelegate = new SendOrPostCallback(OnLogOutCompleted);
            }
            base.InvokeAsync(onBeginLogOutDelegate, new object[]
                                                        {
                                                            prmSession
                                                        }, onEndLogOutDelegate, onLogOutCompletedDelegate, userState);
        }

        private IAsyncResult OnBeginIsValidSession(object[] inValues, AsyncCallback callback, object asyncState)
        {
            string prmSessionId = ((string) (inValues[0]));
            return ((ILoginService) (this)).BeginIsValidSession(prmSessionId, callback, asyncState);
        }

        private object[] OnEndIsValidSession(IAsyncResult result)
        {
            bool retVal = ((ILoginService) (this)).EndIsValidSession(result);
            return new object[]
                       {
                           retVal
                       };
        }

        private void OnIsValidSessionCompleted(object state)
        {
            if ((IsValidSessionCompleted != null))
            {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs) (state));
                IsValidSessionCompleted(this,
                                        new IsValidSessionCompletedEventArgs(e.Results, e.Error, e.Cancelled,
                                                                             e.UserState));
            }
        }

        public void IsValidSessionAsync(string prmSessionId)
        {
            IsValidSessionAsync(prmSessionId, null);
        }

        public void IsValidSessionAsync(string prmSessionId, object userState)
        {
            if ((onBeginIsValidSessionDelegate == null))
            {
                onBeginIsValidSessionDelegate = new BeginOperationDelegate(OnBeginIsValidSession);
            }
            if ((onEndIsValidSessionDelegate == null))
            {
                onEndIsValidSessionDelegate = new EndOperationDelegate(OnEndIsValidSession);
            }
            if ((onIsValidSessionCompletedDelegate == null))
            {
                onIsValidSessionCompletedDelegate = new SendOrPostCallback(OnIsValidSessionCompleted);
            }
            base.InvokeAsync(onBeginIsValidSessionDelegate, new object[]
                                                                {
                                                                    prmSessionId
                                                                }, onEndIsValidSessionDelegate,
                             onIsValidSessionCompletedDelegate, userState);
        }

        private IAsyncResult OnBeginListaEmpresas(object[] inValues, AsyncCallback callback, object asyncState)
        {
            return ((ILoginService) (this)).BeginListaEmpresas(callback, asyncState);
        }

        private object[] OnEndListaEmpresas(IAsyncResult result)
        {
            ObservableCollection<Empresa> retVal = ((ILoginService) (this)).EndListaEmpresas(result);
            return new object[]
                       {
                           retVal
                       };
        }

        private void OnListaEmpresasCompleted(object state)
        {
            if ((ListaEmpresasCompleted != null))
            {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs) (state));
                ListaEmpresasCompleted(this,
                                       new ListaEmpresasCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
            }
        }

        public void ListaEmpresasAsync()
        {
            ListaEmpresasAsync(null);
        }

        public void ListaEmpresasAsync(object userState)
        {
            if ((onBeginListaEmpresasDelegate == null))
            {
                onBeginListaEmpresasDelegate = new BeginOperationDelegate(OnBeginListaEmpresas);
            }
            if ((onEndListaEmpresasDelegate == null))
            {
                onEndListaEmpresasDelegate = new EndOperationDelegate(OnEndListaEmpresas);
            }
            if ((onListaEmpresasCompletedDelegate == null))
            {
                onListaEmpresasCompletedDelegate = new SendOrPostCallback(OnListaEmpresasCompleted);
            }
            base.InvokeAsync(onBeginListaEmpresasDelegate, null, onEndListaEmpresasDelegate,
                             onListaEmpresasCompletedDelegate, userState);
        }

        private IAsyncResult OnBeginOpen(object[] inValues, AsyncCallback callback, object asyncState)
        {
            return ((ICommunicationObject) (this)).BeginOpen(callback, asyncState);
        }

        private object[] OnEndOpen(IAsyncResult result)
        {
            ((ICommunicationObject) (this)).EndOpen(result);
            return null;
        }

        private void OnOpenCompleted(object state)
        {
            if ((OpenCompleted != null))
            {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs) (state));
                OpenCompleted(this, new AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }

        public void OpenAsync()
        {
            OpenAsync(null);
        }

        public void OpenAsync(object userState)
        {
            if ((onBeginOpenDelegate == null))
            {
                onBeginOpenDelegate = new BeginOperationDelegate(OnBeginOpen);
            }
            if ((onEndOpenDelegate == null))
            {
                onEndOpenDelegate = new EndOperationDelegate(OnEndOpen);
            }
            if ((onOpenCompletedDelegate == null))
            {
                onOpenCompletedDelegate = new SendOrPostCallback(OnOpenCompleted);
            }
            base.InvokeAsync(onBeginOpenDelegate, null, onEndOpenDelegate, onOpenCompletedDelegate, userState);
        }

        private IAsyncResult OnBeginClose(object[] inValues, AsyncCallback callback, object asyncState)
        {
            return ((ICommunicationObject) (this)).BeginClose(callback, asyncState);
        }

        private object[] OnEndClose(IAsyncResult result)
        {
            ((ICommunicationObject) (this)).EndClose(result);
            return null;
        }

        private void OnCloseCompleted(object state)
        {
            if ((CloseCompleted != null))
            {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs) (state));
                CloseCompleted(this, new AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }

        public void CloseAsync()
        {
            CloseAsync(null);
        }

        public void CloseAsync(object userState)
        {
            if ((onBeginCloseDelegate == null))
            {
                onBeginCloseDelegate = new BeginOperationDelegate(OnBeginClose);
            }
            if ((onEndCloseDelegate == null))
            {
                onEndCloseDelegate = new EndOperationDelegate(OnEndClose);
            }
            if ((onCloseCompletedDelegate == null))
            {
                onCloseCompletedDelegate = new SendOrPostCallback(OnCloseCompleted);
            }
            base.InvokeAsync(onBeginCloseDelegate, null, onEndCloseDelegate, onCloseCompletedDelegate, userState);
        }

        protected override ILoginService CreateChannel()
        {
            return new LoginServiceClientChannel(this);
        }

        #region Nested type: LoginServiceClientChannel

        private class LoginServiceClientChannel : ChannelBase<ILoginService>, ILoginService
        {
            public LoginServiceClientChannel(ClientBase<ILoginService> client) :
                base(client)
            {
            }

            #region ILoginService Members

            public IAsyncResult BeginLogin(UserCredentials prmCredentials, AsyncCallback callback, object asyncState)
            {
                object[] _args = new object[1];
                _args[0] = prmCredentials;
                IAsyncResult _result = base.BeginInvoke("Login", _args, callback, asyncState);
                return _result;
            }

            public UserData EndLogin(IAsyncResult result)
            {
                object[] _args = new object[0];
                UserData _result = ((UserData) (base.EndInvoke("Login", _args, result)));
                return _result;
            }

            public IAsyncResult BeginLogOut(string prmSession, AsyncCallback callback, object asyncState)
            {
                object[] _args = new object[1];
                _args[0] = prmSession;
                IAsyncResult _result = base.BeginInvoke("LogOut", _args, callback, asyncState);
                return _result;
            }

            public int EndLogOut(IAsyncResult result)
            {
                object[] _args = new object[0];
                int _result = ((int) (base.EndInvoke("LogOut", _args, result)));
                return _result;
            }

            public IAsyncResult BeginIsValidSession(string prmSessionId, AsyncCallback callback, object asyncState)
            {
                object[] _args = new object[1];
                _args[0] = prmSessionId;
                IAsyncResult _result = base.BeginInvoke("IsValidSession", _args, callback, asyncState);
                return _result;
            }

            public bool EndIsValidSession(IAsyncResult result)
            {
                object[] _args = new object[0];
                bool _result = ((bool) (base.EndInvoke("IsValidSession", _args, result)));
                return _result;
            }

            public IAsyncResult BeginListaEmpresas(AsyncCallback callback, object asyncState)
            {
                object[] _args = new object[0];
                IAsyncResult _result = base.BeginInvoke("ListaEmpresas", _args, callback, asyncState);
                return _result;
            }

            public ObservableCollection<Empresa> EndListaEmpresas(IAsyncResult result)
            {
                object[] _args = new object[0];
                ObservableCollection<Empresa> _result =
                    ((ObservableCollection<Empresa>) (base.EndInvoke("ListaEmpresas", _args, result)));
                return _result;
            }

            #endregion
        }

        #endregion
    }
}
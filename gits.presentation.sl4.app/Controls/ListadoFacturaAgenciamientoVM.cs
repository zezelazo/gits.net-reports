﻿#region

using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using gits.distributedservices.facturacion.dto;
using gits.presentation.sl4.app.Services;
using gits.presentation.sl4.app.svcFacturacion;

#endregion

namespace gits.presentation.sl4.app.Controls
{
    public class ListadoFacturaAgenciamientoVM : INotifyPropertyChanged
    {
        private IEnumerable<FacturaAgenciamiento> _resultado;

        public ListadoFacturaAgenciamientoVM()
        {
            AppServices.Current.ServicioFacturacion.GetListadoFacturaAgenciamientoCompleted +=
                ServicioFacturacionGetListadoFacturaAgenciamientoCompleted;
        }

        public IEnumerable<FacturaAgenciamiento> Resultado
        {
            get { return _resultado; }
            set
            {
                if (_resultado == value) return;
                _resultado = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Resultado"));
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        private void ServicioFacturacionGetListadoFacturaAgenciamientoCompleted(object sender,
                                                                                GetListadoFacturaAgenciamientoCompletedEventArgs
                                                                                    e)
        {
            XmlDictionaryReader binaryDictionaryReader =
                XmlDictionaryReader.CreateBinaryReader(new MemoryStream(e.Result),
                                                       XmlDictionaryReaderQuotas.Max);
            var serializer = new DataContractSerializer(typeof (IEnumerable<FacturaAgenciamiento>));
            var rpta = serializer.ReadObject(binaryDictionaryReader) as IEnumerable<FacturaAgenciamiento>;
            Resultado = rpta;
        }
    }
}
﻿#region

using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using GalaSoft.MvvmLight.Messaging;
using gits.distributedservices.facturacion.dto;
using Syncfusion.XlsIO;
using System.Collections;

#endregion

namespace gits.presentation.sl4.app.Controls
{
    public partial class ListadoFacturaClienteDetallado
    {
        public ListadoFacturaClienteDetallado()
        {
            InitializeComponent();
            Messenger.Default.Register<ReportMsg>(this, GenerateReport);
        }

        private void GenerateReport(ReportMsg code)
        {
            if (code.Report != "Detallado") return; 

            const int dataIniRow = 8;
             
                var excelEngine = new ExcelEngine();
                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Excel97to2003;

                IWorkbook workbook = application.Workbooks.Create(1);
                IWorksheet sheet = workbook.Worksheets[0];
                sheet.Name = "Listado Factura Clientes Detallado";
                sheet.Range["A4"].Value = "Listado Factura Clientes Detallado";
                sheet.Range["A4:AB4"].Merge();
                SetTitleStyle(sheet.Range["A4:AB4"]);
                sheet.Range["A5:J5"].Merge();
                sheet.Range["A5:J5"].Value = "Comprobante Pago";
                SetTitleStyle(sheet.Range["A5:J5"]);
                sheet.Range["K5:AB5"].Merge();
                sheet.Range["K5:AB5"].Value = "Documento";
                SetTitleStyle(sheet.Range["K5:AB5"]);

                sheet.Range["A6"].Value = "Pago";
                sheet.Range["A6:A7"].Merge();
                SetTitleStyle(sheet.Range["A6:A7"]);
                sheet.Range["B6"].Value = "Numero";
                sheet.Range["B6:B7"].Merge();
                SetTitleStyle(sheet.Range["B6:B7"]);
                sheet.Range["C6"].Value = "Banco";
                sheet.Range["C6:C7"].Merge();
                SetTitleStyle(sheet.Range["C6:C7"]);
                sheet.Range["D6"].Value = "Fecha de Pago";
                sheet.Range["D6:D7"].Merge();
                SetTitleStyle(sheet.Range["D6:D7"]);
                sheet.Range["E6"].Value = "Moneda";
                sheet.Range["E6:E7"].Merge();
                SetTitleStyle(sheet.Range["E6:E7"]);
                sheet.Range["F6"].Value = "Ruc Agente";
                sheet.Range["F6:F7"].Merge();
                SetTitleStyle(sheet.Range["F6:F7"]);
                sheet.Range["G6"].Value = "Agente";
                sheet.Range["G6:G7"].Merge();
                SetTitleStyle(sheet.Range["G6:G7"]);
                sheet.Range["H6"].Value = "Cambio";
                sheet.Range["H6:H7"].Merge();
                SetTitleStyle(sheet.Range["H6:H7"]);
                sheet.Range["I6"].Value = "Retencion";
                sheet.Range["I6:I7"].Merge();
                SetTitleStyle(sheet.Range["I6:I7"]);
                sheet.Range["J6"].Value = "Monto";
                sheet.Range["J6:J7"].Merge();
                SetTitleStyle(sheet.Range["J6:J7"]);
                sheet.Range["K6"].Value = "Comprobante";
                sheet.Range["K6:K7"].Merge();
                SetTitleStyle(sheet.Range["K6:K7"]);
                sheet.Range["L6"].Value = "Serie";
                sheet.Range["L6:L7"].Merge();
                SetTitleStyle(sheet.Range["L6:L7"]);
                sheet.Range["M6"].Value = "Numero";
                sheet.Range["M6:M7"].Merge();
                SetTitleStyle(sheet.Range["M6:M7"]);
                sheet.Range["N6"].Value = "Fecha Registro";
                sheet.Range["N6:N7"].Merge();
                SetTitleStyle(sheet.Range["N6:N7"]);
                sheet.Range["O6"].Value = "Ruc";
                sheet.Range["O6:O7"].Merge();
                SetTitleStyle(sheet.Range["O6:O7"]);
                sheet.Range["P6"].Value = "Facturar A:";
                sheet.Range["P6:P7"].Merge();
                SetTitleStyle(sheet.Range["P6:P7"]);


                sheet.Range["Q6"].Value = "Concepto";
                sheet.Range["Q6:Q7"].Merge();
                SetTitleStyle(sheet.Range["Q6:Q7"]);

                sheet.Range["R6"].Value = "Soles";
                sheet.Range["R6:T6"].Merge();
                SetTitleStyle(sheet.Range["R6:T6"]);
                sheet.Range["R7"].Value = "Valor Ventas";
                SetTitleStyle(sheet.Range["R7"]);
                sheet.Range["S7"].Value = "IGV";
                SetTitleStyle(sheet.Range["S7"]);
                sheet.Range["T7"].Value = "Total Factura";
                SetTitleStyle(sheet.Range["T7"]);

                sheet.Range["U6"].Value = "Dolares";
                sheet.Range["U6:W6"].Merge();
                SetTitleStyle(sheet.Range["U6:W6"]);
                sheet.Range["U7"].Value = "Valor Ventas";
                SetTitleStyle(sheet.Range["U7"]);
                sheet.Range["V7"].Value = "IGV";
                SetTitleStyle(sheet.Range["V7"]);
                sheet.Range["W7"].Value = "Total Factura";
                SetTitleStyle(sheet.Range["W7"]);

                sheet.Range["X6"].Value = "BL/M";
                sheet.Range["X6:X7"].Merge();
                SetTitleStyle(sheet.Range["X6:X7"]);
                sheet.Range["Y6"].Value = "BL";
                sheet.Range["Y6:Y7"].Merge();
                SetTitleStyle(sheet.Range["Y6:Y7"]);
                sheet.Range["Z6"].Value = "Nave";
                sheet.Range["Z6:Z7"].Merge();
                SetTitleStyle(sheet.Range["Z6:Z7"]);
                sheet.Range["AA6"].Value = "Viaje";
                sheet.Range["AA6:AA7"].Merge();
                SetTitleStyle(sheet.Range["AA6:AA7"]);
                sheet.Range["AB6"].Value = "Empresa";
                sheet.Range["AB6:AB7"].Merge();
                SetTitleStyle(sheet.Range["AB6:AB7"]);


                int originalPageSize = rdpPager.PageSize;
                int originalPageIndex = rdpPager.PageIndex;
                rdpPager.PageSize = 0;
                var row = dataIniRow;


                foreach (var j in (IEnumerable) rdpPager.PagedSource)
                {
                    var r = j as FacturaDetallada;
                    if (r == null) continue;
                    sheet.Range["A" + row].Value = r.FormaDePago;
                    sheet.Range["B" + row].Value = r.NroDocumentoDePago;
                    sheet.Range["C" + row].Value = r.Banco;
                    sheet.Range["D" + row].Value = r.FechaPago;
                    sheet.Range["E" + row].Value = r.Moneda;
                    sheet.Range["F" + row].Value = r.RucAgente;
                    sheet.Range["G" + row].Value = r.Agente;
                    sheet.Range["H" + row].Value = r.TipoDeCambio.ToString();
                    sheet.Range["I" + row].Value = r.Retencion.ToString();
                    sheet.Range["J" + row].Value = r.Monto.ToString();
                    sheet.Range["K" + row].Value = r.Comprobante;
                    sheet.Range["L" + row].Value = r.Serie;
                    sheet.Range["M" + row].Value = r.Numero;
                    sheet.Range["N" + row].Value = r.FechaRegistro;
                    sheet.Range["O" + row].Value = r.Ruc;
                    sheet.Range["P" + row].Value = r.FacturarA;
                    sheet.Range["Q" + row].Value = r.DescripcionDelServicio;
                    sheet.Range["R" + row].Value = r.FacturaMontoBruto.ToString();
                    sheet.Range["S" + row].Value = r.FacturaMontoImpuesto.ToString();
                    sheet.Range["T" + row].Value = r.FacturaMontoNeto.ToString();
                    sheet.Range["U" + row].Value = r.FacturaMontoBrutoDolares.ToString();
                    sheet.Range["V" + row].Value = r.FacturaMontoImpuestoDolares.ToString();
                    sheet.Range["W" + row].Value = r.FacturaMontoNetoDolares.ToString();
                    sheet.Range["X" + row].Value = r.BL_M;
                    sheet.Range["Y" + row].Value = r.BL;
                    sheet.Range["Z" + row].Value = r.Nave;
                    sheet.Range["AA" + row].Value = r.Viaje;
                    sheet.Range["AB" + row].Value = r.Empresa;
                    row ++;
                }
                row--;

                SetDetailsStyle(sheet.Range["A" + dataIniRow + ":A" + row], ExcelHAlign.HAlignCenter, 15, "@");
                SetDetailsStyle(sheet.Range["B" + dataIniRow + ":B" + row], ExcelHAlign.HAlignRight, 11, "0000000000");
                SetDetailsStyle(sheet.Range["C" + dataIniRow + ":C" + row], ExcelHAlign.HAlignCenter, 8, "@");
                SetDetailsStyle(sheet.Range["D" + dataIniRow + ":D" + row], ExcelHAlign.HAlignCenter, 14, "dd/mm/yyyy");
                SetDetailsStyle(sheet.Range["E" + dataIniRow + ":E" + row], ExcelHAlign.HAlignCenter, 13, "@");
                SetDetailsStyle(sheet.Range["F" + dataIniRow + ":F" + row], ExcelHAlign.HAlignCenter, 14, "00000000000");
                SetDetailsStyle(sheet.Range["G" + dataIniRow + ":G" + row], ExcelHAlign.HAlignLeft, 46, "@");
                SetDetailsStyle(sheet.Range["H" + dataIniRow + ":H" + row], ExcelHAlign.HAlignRight, 7, "0.000");
                SetDetailsStyle(sheet.Range["I" + dataIniRow + ":I" + row], ExcelHAlign.HAlignRight, 10, "0.000");
                SetDetailsStyle(sheet.Range["J" + dataIniRow + ":J" + row], ExcelHAlign.HAlignRight, 8, "0.000");
                SetDetailsStyle(sheet.Range["K" + dataIniRow + ":K" + row], ExcelHAlign.HAlignCenter, 13, "@");
                SetDetailsStyle(sheet.Range["L" + dataIniRow + ":L" + row], ExcelHAlign.HAlignCenter, 7, "000");
                SetDetailsStyle(sheet.Range["M" + dataIniRow + ":M" + row], ExcelHAlign.HAlignRight, 8, "0000000");
                SetDetailsStyle(sheet.Range["N" + dataIniRow + ":N" + row], ExcelHAlign.HAlignCenter, 14, "dd/mm/yyyy");
                SetDetailsStyle(sheet.Range["O" + dataIniRow + ":O" + row], ExcelHAlign.HAlignCenter, 14, "00000000000");
                SetDetailsStyle(sheet.Range["P" + dataIniRow + ":P" + row], ExcelHAlign.HAlignLeft, 46, "@");
                SetDetailsStyle(sheet.Range["Q" + dataIniRow + ":Q" + row], ExcelHAlign.HAlignRight, 30, "@");
                SetDetailsStyle(sheet.Range["R" + dataIniRow + ":R" + row], ExcelHAlign.HAlignRight, 13, "0.000");
                SetDetailsStyle(sheet.Range["S" + dataIniRow + ":S" + row], ExcelHAlign.HAlignRight, 13, "0.000");
                SetDetailsStyle(sheet.Range["T" + dataIniRow + ":T" + row], ExcelHAlign.HAlignRight, 13, "0.000");
                SetDetailsStyle(sheet.Range["U" + dataIniRow + ":U" + row], ExcelHAlign.HAlignRight, 13, "0.000");
                SetDetailsStyle(sheet.Range["V" + dataIniRow + ":V" + row], ExcelHAlign.HAlignRight, 13, "0.000");
                SetDetailsStyle(sheet.Range["W" + dataIniRow + ":W" + row], ExcelHAlign.HAlignRight, 13, "0.000");
                SetDetailsStyle(sheet.Range["X" + dataIniRow + ":X" + row], ExcelHAlign.HAlignRight, 20, "@");
                SetDetailsStyle(sheet.Range["Y" + dataIniRow + ":Y" + row], ExcelHAlign.HAlignRight, 20, "@");
                SetDetailsStyle(sheet.Range["Z" + dataIniRow + ":Z" + row], ExcelHAlign.HAlignRight, 17, "@");
                SetDetailsStyle(sheet.Range["AA" + dataIniRow + ":AA" + row], ExcelHAlign.HAlignCenter, 10, "@");
                SetDetailsStyle(sheet.Range["AB" + dataIniRow + ":AB" + row], ExcelHAlign.HAlignCenter, 10, "@");


                sheet.Range["A" + dataIniRow + ":AB" + row].BorderAround(ExcelLineStyle.Medium);

                sheet.Range["A" + dataIniRow + ":AB" + row].Borders[ExcelBordersIndex.DiagonalUp].LineStyle =
                    ExcelLineStyle.None;
                sheet.Range["A" + dataIniRow + ":AB" + row].Borders[ExcelBordersIndex.DiagonalDown].LineStyle =
                    ExcelLineStyle.None;

                rdpPager.PageSize = originalPageSize;
                rdpPager.PageIndex = originalPageIndex;

                    workbook.SaveAs(code.Stream);
            workbook.Close();
            excelEngine.Dispose();
            code.Stream.Dispose();
            code = null;

                MessageBox.Show("El reporte fue generado satisfactoriamente", "Gits.NET", MessageBoxButton.OK);
             
        }

        private void SetTitleStyle(IRange range)
        {
            range.CellStyle.Font.Bold = true;
            range.CellStyle.HorizontalAlignment = ExcelHAlign.HAlignCenter;
            range.CellStyle.VerticalAlignment = ExcelVAlign.VAlignCenter;
            range.CellStyle.Color = Color.FromArgb(255, 183, 222, 232);

            range.BorderAround(ExcelLineStyle.Medium);
            range.Borders[ExcelBordersIndex.DiagonalUp].LineStyle = ExcelLineStyle.None;
            range.Borders[ExcelBordersIndex.DiagonalDown].LineStyle = ExcelLineStyle.None;
        }

        private void SetDetailsStyle(IRange range, ExcelHAlign hAlign, double size, string format)
        {
            range.ColumnWidth = size;
            range.NumberFormat = format;
            range.HorizontalAlignment = hAlign;
            range.VerticalAlignment = ExcelVAlign.VAlignCenter;
            range.BorderInside(ExcelLineStyle.Thin);
            range.BorderAround(ExcelLineStyle.Thin);

            range.Borders[ExcelBordersIndex.DiagonalUp].LineStyle = ExcelLineStyle.None;
            range.Borders[ExcelBordersIndex.DiagonalDown].LineStyle = ExcelLineStyle.None;
        }
    }
}
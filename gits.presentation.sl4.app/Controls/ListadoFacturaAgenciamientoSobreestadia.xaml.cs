﻿#region

using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using GalaSoft.MvvmLight.Messaging;
using gits.distributedservices.facturacion.dto;
using Syncfusion.XlsIO;

#endregion

namespace gits.presentation.sl4.app.Controls
{
    public partial class ListadoFacturaAgenciamientoSobreestadia : UserControl
    {
        public ListadoFacturaAgenciamientoSobreestadia()
        {
            InitializeComponent();
            Messenger.Default.Register<ReportMsg>(this, GenerateReport);
        }

        private void GenerateReport(ReportMsg code)
        {
            if (code.Report != "Sobreestadia") return;

            var dataIniRow = 7;

            var excelEngine = new ExcelEngine();
            IApplication application = excelEngine.Excel;
            application.DefaultVersion = ExcelVersion.Excel97to2003;

            IWorkbook workbook = application.Workbooks.Create(1);
            IWorksheet sheet = workbook.Worksheets[0];

            sheet.Name = "Listado Factura Agenciamiento Sobreestadia";
            sheet.Range["A5"].Value = "Listado Factura Agenciamiento Sobreestadia";
            sheet.Range["A5:M5"].Merge();

            SetTitleStyle(sheet.Range["A5:M5"]);

            sheet.Range["A6"].Value = "Fecha";
            sheet.Range["B6"].Value = "Cheque";
            sheet.Range["C6"].Value = "Invoice";
            sheet.Range["D6"].Value = "Tipo Cobro";
            sheet.Range["E6"].Value = "TotalCheque";
            sheet.Range["F6"].Value = "Neto";
            sheet.Range["G6"].Value = "Retencion";
            sheet.Range["H6"].Value = "Total";
            sheet.Range["I6"].Value = "Desde Hasta";
            sheet.Range["J6"].Value = "RUC";
            sheet.Range["K6"].Value = "Cliente";
            sheet.Range["L6"].Value = "Banco";
            sheet.Range["M6"].Value = "B/L";

            SetTitleStyle(sheet.Range["A6"]);
            SetTitleStyle(sheet.Range["B6"]);
            SetTitleStyle(sheet.Range["C6"]);
            SetTitleStyle(sheet.Range["D6"]);
            SetTitleStyle(sheet.Range["E6"]);
            SetTitleStyle(sheet.Range["F6"]);
            SetTitleStyle(sheet.Range["G6"]);
            SetTitleStyle(sheet.Range["H6"]);
            SetTitleStyle(sheet.Range["I6"]);
            SetTitleStyle(sheet.Range["J6"]);
            SetTitleStyle(sheet.Range["K6"]);
            SetTitleStyle(sheet.Range["L6"]);
            SetTitleStyle(sheet.Range["M6"]);


            SetTitleStyle(sheet.Range["A6:M6"]);

            int originalPageSize = rdpPager.PageSize;
            int originalPageIndex = rdpPager.PageIndex;

            rdpPager.PageSize = 0;

            var row = dataIniRow;

            foreach (var j in (IEnumerable)rdpPager.PagedSource)
            {
                var r = j as FacturaAgenciamientoSobreestadia;
                if (r == null) continue;
                sheet.Range["A" + row].Value = r.Fecha.ToString();
                sheet.Range["B" + row].Value = r.Cheque;
                sheet.Range["C" + row].Value = r.Invoice;
                sheet.Range["D" + row].Value = r.TipoDeCobro;
                sheet.Range["E" + row].Value = r.TotalCheque.ToString();
                sheet.Range["F" + row].Value = r.Neto.ToString();
                sheet.Range["G" + row].Value = r.Retencion.ToString();
                sheet.Range["H" + row].Value = r.Total.ToString();
                sheet.Range["I" + row].Value = r.DesdeHasta;
                sheet.Range["J" + row].Value = r.Ruc;
                sheet.Range["K" + row].Value = r.Cliente;
                sheet.Range["L" + row].Value = r.Banco;
                sheet.Range["M" + row].Value = r.BL;

                row++;
            }
            row--;

            SetDetailsStyle(sheet.Range["A" + dataIniRow + ":A" + row], ExcelHAlign.HAlignCenter, 15, "dd/mm/yyyy");
            SetDetailsStyle(sheet.Range["B" + dataIniRow + ":B" + row], ExcelHAlign.HAlignRight, 25, "@");
            SetDetailsStyle(sheet.Range["C" + dataIniRow + ":C" + row], ExcelHAlign.HAlignRight, 11, "0000000000");
            SetDetailsStyle(sheet.Range["D" + dataIniRow + ":D" + row], ExcelHAlign.HAlignCenter, 14, "@");
            SetDetailsStyle(sheet.Range["E" + dataIniRow + ":E" + row], ExcelHAlign.HAlignRight, 13, "0.000");
            SetDetailsStyle(sheet.Range["F" + dataIniRow + ":F" + row], ExcelHAlign.HAlignRight, 13, "0.000");
            SetDetailsStyle(sheet.Range["G" + dataIniRow + ":G" + row], ExcelHAlign.HAlignRight, 13, "0.000");
            SetDetailsStyle(sheet.Range["I" + dataIniRow + ":I" + row], ExcelHAlign.HAlignCenter, 36, "@");
            SetDetailsStyle(sheet.Range["J" + dataIniRow + ":J" + row], ExcelHAlign.HAlignRight, 14, "00000000000");
            SetDetailsStyle(sheet.Range["K" + dataIniRow + ":K" + row], ExcelHAlign.HAlignCenter, 56, "@");
            SetDetailsStyle(sheet.Range["L" + dataIniRow + ":L" + row], ExcelHAlign.HAlignCenter, 36, "@");
            SetDetailsStyle(sheet.Range["M" + dataIniRow + ":M" + row], ExcelHAlign.HAlignRight, 20, "@");


            sheet.Range["A" + dataIniRow + ":M" + row].BorderAround(ExcelLineStyle.Medium);

            sheet.Range["A" + dataIniRow + ":M" + row].Borders[ExcelBordersIndex.DiagonalUp].LineStyle =
                ExcelLineStyle.None;
            sheet.Range["A" + dataIniRow + ":M" + row].Borders[ExcelBordersIndex.DiagonalDown].LineStyle =
                ExcelLineStyle.None;

            rdpPager.PageSize = originalPageSize;
            rdpPager.PageIndex = originalPageIndex;

            workbook.SaveAs(code.Stream);
            workbook.Close();
            excelEngine.Dispose();
            code.Stream.Dispose();
            code = null;

            MessageBox.Show("El reporte fue generado satisfactoriamente", "Gits.NET", MessageBoxButton.OK);

        }

        private void SetTitleStyle(IRange range)
        {
            range.CellStyle.Font.Bold = true;
            range.CellStyle.HorizontalAlignment = ExcelHAlign.HAlignCenter;
            range.CellStyle.VerticalAlignment = ExcelVAlign.VAlignCenter;
            range.CellStyle.Color = Color.FromArgb(255, 183, 222, 232);

            range.BorderAround(ExcelLineStyle.Medium);
            range.Borders[ExcelBordersIndex.DiagonalUp].LineStyle = ExcelLineStyle.None;
            range.Borders[ExcelBordersIndex.DiagonalDown].LineStyle = ExcelLineStyle.None;
        }

        private void SetDetailsStyle(IRange range, ExcelHAlign hAlign, double size, string format)
        {
            range.ColumnWidth = size;
            range.NumberFormat = format;
            range.HorizontalAlignment = hAlign;
            range.VerticalAlignment = ExcelVAlign.VAlignCenter;
            range.BorderInside(ExcelLineStyle.Thin);
            range.BorderAround(ExcelLineStyle.Thin);

            range.Borders[ExcelBordersIndex.DiagonalUp].LineStyle = ExcelLineStyle.None;
            range.Borders[ExcelBordersIndex.DiagonalDown].LineStyle = ExcelLineStyle.None;
        }
    }
}
﻿#region

using System.Collections.ObjectModel;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using gits.presentation.sl4.app.Services;
using gits.presentation.sl4.app.svcSeguridad;

#endregion

namespace gits.presentation.sl4.app.Controls
{
    public class LoginControlVM : ViewModelBase
    {
        private string _clave;
        private Empresa _empresa;
        private ObservableCollection<Empresa> _empresas;
        private string _nombre;
        private int _paso;
        private string _usuario;
        private Visibility _verVolver = Visibility.Collapsed;

        public LoginControlVM()
        {
            if (!IsInDesignMode)
            {
                SecurityService.Current.LoginService.LoginCompleted += LoginServiceLoginCompleted;
                SecurityService.Current.LoginService.LogOutCompleted += LoginServiceLogOutCompleted;
                SecurityService.Current.LoginService.ListaEmpresasCompleted += (s, e) => { Empresas = e.Result; };
                SecurityService.Current.LoginService.ListaEmpresasAsync();
                LogOut =
                    new RelayCommand(
                        () =>
                        SecurityService.Current.LoginService.LogOutAsync(
                            SecurityService.Current.CurrentUser.SessionCode.ToString()));
            }

            Next = new RelayCommand(() =>
                                        {
                                            switch (Paso)
                                            {
                                                case 1:
                                                    if (Empresa == null) return;
                                                    Paso = 2;
                                                    break;
                                                case 2:
                                                    if (Usuario.Trim().Length < 2) return;
                                                    Paso = 3;
                                                    break;
                                                case 3:
                                                    if (Clave.Trim().Length < 2) return;
                                                    ValidarDatos();
                                                    break;
                                            }
                                        });

            Prev = new RelayCommand(() =>
                                        {
                                            switch (Paso)
                                            {
                                                case 2:
                                                    Paso = 1;
                                                    Usuario = string.Empty;
                                                    break;
                                                case 3:
                                                    Paso = 2;
                                                    Clave = string.Empty;
                                                    break;
                                            }
                                        });


            Paso = 1;
        }

        public RelayCommand Next { get; private set; }
        public RelayCommand Prev { get; private set; }
        public RelayCommand LogOut { get; private set; }

        public ObservableCollection<Empresa> Empresas
        {
            get { return _empresas; }
            set
            {
                if (_empresas == value) return;
                _empresas = value;
                RaisePropertyChanged("Empresas");
            }
        }

        public Empresa Empresa
        {
            get { return _empresa; }
            set
            {
                if (_empresa == value) return;
                _empresa = value;
                RaisePropertyChanged("Empresa");
            }
        }

        public string Clave
        {
            get { return _clave; }
            set
            {
                if (_clave == value) return;
                _clave = value;
                RaisePropertyChanged("Clave");
            }
        }

        public string Usuario
        {
            get { return _usuario; }
            set
            {
                if (_usuario == value) return;
                _usuario = value;
                RaisePropertyChanged("Usuario");
            }
        }

        public string Nombre
        {
            get { return _nombre; }
            set
            {
                if (_nombre == value) return;
                _nombre = value;
                RaisePropertyChanged("Nombre");
            }
        }

        public int Paso
        {
            get { return _paso; }
            set
            {
                if (_paso == value) return;
                _paso = value;

                switch (_paso)
                {
                    case 1:
                        Messenger.Default.Send<string>("Empresa");
                        break;
                    case 2:
                        Messenger.Default.Send<string>("Usuario");
                        break;
                    case 3:
                        Messenger.Default.Send<string>("Password");
                        break;
                }
                _verVolver = _paso >= 2 ? Visibility.Visible : Visibility.Collapsed;

                RaisePropertyChanged("Paso");
                RaisePropertyChanged("VerVolver");
            }
        }

        public Visibility VerVolver
        {
            get { return _verVolver; }
        }

        public void ValidarDatos()
        {
            var credentials = new UserCredentials
                                  {
                                      ClientIp = "",
                                      ClientName = "",
                                      ClientType = ClientsTypesEnum.Silverlight,
                                      UserCompany = Empresa.Id,
                                      UserName = Usuario,
                                      UserPassword = Clave
                                  };


            SecurityService.Current.LoginService.LoginAsync(credentials);
        }

        private void LoginServiceLogOutCompleted(object sender, LogOutCompletedEventArgs e)
        {
            Empresa = null;
            Usuario = string.Empty;
            Clave = string.Empty;
            Nombre = string.Empty;
            Paso = 1;
        }

        private void LoginServiceLoginCompleted(object sender, LoginCompletedEventArgs e)
        {
            if (e.Result.LoginError == null)
            {
                Nombre = e.Result.UserRealName;
                Paso = 4;
                return;
            }

            switch (e.Result.LoginError.ErrCode)
            {
                case 2:
                    Paso = 2;
                    Clave = string.Empty;
                    Usuario = string.Empty;
                    break;
                case 3:
                    Paso = 3;
                    Clave = string.Empty;
                    break;
                case 4:
                    Paso = 1;
                    Usuario = string.Empty;
                    Clave = string.Empty;
                    break;
                case 1:
                    Paso = 1;
                    Empresa = null;
                    Usuario = string.Empty;
                    Clave = string.Empty;
                    break;
            }
        }
    }
}
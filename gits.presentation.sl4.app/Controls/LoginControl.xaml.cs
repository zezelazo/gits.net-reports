﻿using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight.Messaging;
using Telerik.Windows.Controls;

namespace gits.presentation.sl4.app.Controls
{
    public partial class LoginControl
    {
        public LoginControl()
        {
            InitializeComponent();
            Messenger.Default.Register(this, (string param) =>
                                                {
                                                    switch (param)
                                                    {
                                                        case "Empresa":
                                                            cboEmpresa.Focus();
                                                            break;
                                                        case "Usuario":
                                                            txtUsuario.Focus();
                                                            break;
                                                        case "Password":
                                                            txtPassword.Focus();
                                                            break;
                                                    }
                                                });
        }


        private void GotoNextStep(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            var x = sender as RadComboBox;
            if (x != null)
            {
                x.GetBindingExpression(RadComboBox.SelectedItemProperty).UpdateSource();
            }
            else
            {
                var y = sender as TextBox;
                if (y != null)
                {
                     y.GetBindingExpression(TextBox.TextProperty).UpdateSource();
                }
                else
                {
                    var w = sender as PasswordBox;
                    if (w != null)
                    {
                        w.GetBindingExpression(PasswordBox.PasswordProperty).UpdateSource();
                        
                    }
                }

            }
 ((LoginControlVM)DataContext).Next.Execute(null);




        }
    }
}

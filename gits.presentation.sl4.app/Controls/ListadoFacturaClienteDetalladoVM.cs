﻿#region

using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using gits.distributedservices.facturacion.dto;
using gits.presentation.sl4.app.Services;
using gits.presentation.sl4.app.svcFacturacion;

#endregion

namespace gits.presentation.sl4.app.Controls
{
    public class ListadoFacturaClienteDetalladoVM : INotifyPropertyChanged
    {
        private IEnumerable<FacturaDetallada> _resultado;

        public ListadoFacturaClienteDetalladoVM()
        {
            AppServices.Current.ServicioFacturacion.GetListadoFacturaDetalladoCompleted +=
                ServicioFacturacionGetListadoFacturaDetalladoCompleted;
            AppServices.Current.ServicioFacturacion.GetListadoFacturaDetalladoSoloDetalleCompleted += 
                ServicioFacturacionGetListadoFacturaDetalladoSoloDetalleCompleted;
        }

        void ServicioFacturacionGetListadoFacturaDetalladoSoloDetalleCompleted(object sender, GetListadoFacturaDetalladoSoloDetalleCompletedEventArgs e)
        {
            XmlDictionaryReader binaryDictionaryReader =
                XmlDictionaryReader.CreateBinaryReader(new MemoryStream(e.Result),
                                                       XmlDictionaryReaderQuotas.Max);
            var serializer = new DataContractSerializer(typeof(IEnumerable<FacturaDetallada>));
            var rpta = serializer.ReadObject(binaryDictionaryReader) as IEnumerable<FacturaDetallada>;
            Resultado = rpta;
        }

        public IEnumerable<FacturaDetallada> Resultado
        {
            get { return _resultado; }
            set
            {
                if (_resultado == value) return;
                _resultado = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Resultado"));
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        private void ServicioFacturacionGetListadoFacturaDetalladoCompleted(object sender,
                                                                            GetListadoFacturaDetalladoCompletedEventArgs
                                                                                e)
        {
            XmlDictionaryReader binaryDictionaryReader =
                XmlDictionaryReader.CreateBinaryReader(new MemoryStream(e.Result),
                                                       XmlDictionaryReaderQuotas.Max);
            var serializer = new DataContractSerializer(typeof (IEnumerable<FacturaDetallada>));
            var rpta = serializer.ReadObject(binaryDictionaryReader) as IEnumerable<FacturaDetallada>;
            Resultado = rpta;
        }
    }
}
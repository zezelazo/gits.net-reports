﻿#region

using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using gits.distributedservices.facturacion.dto;
using gits.presentation.sl4.app.Services;
using gits.presentation.sl4.app.svcFacturacion;

#endregion

namespace gits.presentation.sl4.app.Controls
{
    public class ListadoFacturaAgenciamientoSobreestadiaVM : INotifyPropertyChanged
    {
        private IEnumerable<FacturaAgenciamientoSobreestadia> _resultado;

        public ListadoFacturaAgenciamientoSobreestadiaVM()
        {
            AppServices.Current.ServicioFacturacion.GetListadoFacturaSobreestadiaCompleted +=
                ServicioFacturacionGetListadoFacturaSobreestadiaCompleted;
        }

        public IEnumerable<FacturaAgenciamientoSobreestadia> Resultado
        {
            get { return _resultado; }
            set
            {
                if (_resultado == value) return;
                _resultado = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Resultado"));
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        private void ServicioFacturacionGetListadoFacturaSobreestadiaCompleted(object sender,
                                                                               GetListadoFacturaSobreestadiaCompletedEventArgs
                                                                                   e)
        {
            XmlDictionaryReader binaryDictionaryReader =
                XmlDictionaryReader.CreateBinaryReader(new MemoryStream(e.Result),
                                                       XmlDictionaryReaderQuotas.Max);
            var serializer = new DataContractSerializer(typeof (IEnumerable<FacturaAgenciamientoSobreestadia>));
            var rpta = serializer.ReadObject(binaryDictionaryReader) as IEnumerable<FacturaAgenciamientoSobreestadia>;
            Resultado = rpta;
        }
    }
}
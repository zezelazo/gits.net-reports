﻿using System.IO;

namespace gits.presentation.sl4.app.Controls
{
	public class ReportMsg
	{
	    public string Report {get; set ; }

	    public Stream Stream  { get; set ;  }
	}
}

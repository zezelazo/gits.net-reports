﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using GalaSoft.MvvmLight;
using gits.distributedservices.facturacion.dto;
using gits.presentation.sl4.app.Services;
using gits.presentation.sl4.app.svcFacturacion;

namespace gits.presentation.sl4.app.Controls
{
    public class ListadoFacturaVM : INotifyPropertyChanged
    {
        public ListadoFacturaVM()
        { 
            AppServices.Current.ServicioFacturacion.GetListadoFacturaAgenciamientoCompleted += ServicioFacturacionGetListadoFacturaAgenciamientoCompleted;
            AppServices.Current.ServicioFacturacion.GetListadoFacturaConsolidadoCompleted += ServicioFacturacionGetListadoFacturaConsolidadoCompleted;
            AppServices.Current.ServicioFacturacion.GetListadoFacturaDetalladoCompleted += ServicioFacturacionGetListadoFacturaDetalladoCompleted;
            AppServices.Current.ServicioFacturacion.GetListadoFacturaSobreestadiaCompleted += ServicioFacturacionGetListadoFacturaSobreestadiaCompleted;
        }

        void ServicioFacturacionGetListadoFacturaSobreestadiaCompleted(object sender, GetListadoFacturaSobreestadiaCompletedEventArgs e)
        {
            XmlDictionaryReader binaryDictionaryReader = XmlDictionaryReader.CreateBinaryReader(new MemoryStream(e.Result as byte[]),
                                                                                                   XmlDictionaryReaderQuotas.Max);
            var serializer = new DataContractSerializer(typeof(IEnumerable<FacturaAgenciamientoSobreestadia>));
            var rpta = serializer.ReadObject(binaryDictionaryReader) as IEnumerable<FacturaAgenciamientoSobreestadia>;
            Resultado = rpta;
        }

        void ServicioFacturacionGetListadoFacturaDetalladoCompleted(object sender, GetListadoFacturaDetalladoCompletedEventArgs e)
        {
            XmlDictionaryReader binaryDictionaryReader = XmlDictionaryReader.CreateBinaryReader(new MemoryStream(e.Result as byte[]), 
                                                                                                    XmlDictionaryReaderQuotas.Max);
            var serializer = new DataContractSerializer(typeof(IEnumerable<FacturaDetallada>));
            var rpta = serializer.ReadObject(binaryDictionaryReader) as IEnumerable<FacturaDetallada>;
            Resultado = rpta;
        }

        void ServicioFacturacionGetListadoFacturaConsolidadoCompleted(object sender, GetListadoFacturaConsolidadoCompletedEventArgs e)
        {
            XmlDictionaryReader binaryDictionaryReader = XmlDictionaryReader.CreateBinaryReader(new MemoryStream(e.Result as byte[]),
                                                                                                   XmlDictionaryReaderQuotas.Max);
            var serializer = new DataContractSerializer(typeof(IEnumerable<FacturaConsolidada>));
            var rpta = serializer.ReadObject(binaryDictionaryReader) as IEnumerable<FacturaConsolidada>;
            Resultado = rpta;
        }

        void ServicioFacturacionGetListadoFacturaAgenciamientoCompleted(object sender, GetListadoFacturaAgenciamientoCompletedEventArgs e)
        {
            XmlDictionaryReader binaryDictionaryReader = XmlDictionaryReader.CreateBinaryReader(new MemoryStream(e.Result as byte[]),
                                                                                                   XmlDictionaryReaderQuotas.Max);
            var serializer = new DataContractSerializer(typeof(IEnumerable<FacturaAgenciamiento>));
            var rpta = serializer.ReadObject(binaryDictionaryReader) as IEnumerable<FacturaAgenciamiento>;
            Resultado = rpta;
        }
         
        private Object _resultado;
        public Object Resultado
        {
            get { return _resultado; }
            set
            {
                if (_resultado==value) return;
                _resultado = value;
                if (PropertyChanged!=null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Resultado"));
                
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}

﻿#region

using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using GalaSoft.MvvmLight.Messaging;
using gits.distributedservices.facturacion.dto;
using Syncfusion.XlsIO;

#endregion

namespace gits.presentation.sl4.app.Controls
{
    public partial class ListadoFacturaAgenciamiento
    {
        public ListadoFacturaAgenciamiento()
        {
            InitializeComponent();
            Messenger.Default.Register<ReportMsg>(this, GenerateReport);
        }

        private void GenerateReport(ReportMsg code)
        {
            if (code.Report != "Agenciamiento") return;



            const int mHeaderIniRow = 5;
            const int sHeaderIniRow = 6;

            const int dataIniRow = 7;


            var excelEngine = new ExcelEngine();
            IApplication application = excelEngine.Excel;
            application.DefaultVersion = ExcelVersion.Excel97to2003;

            IWorkbook workbook = application.Workbooks.Create(1);
            IWorksheet sheet = workbook.Worksheets[0];

            sheet.Name = "Listado Factura Agenciamiento";
            sheet.Range["A" + mHeaderIniRow].Value = "Listado Factura Agenciamiento";
            sheet.Range["A" + mHeaderIniRow + ":H" + mHeaderIniRow].Merge();
            SetTitleStyle(sheet.Range["A" + mHeaderIniRow + ":H" + mHeaderIniRow]);


            sheet.Range["A" + sHeaderIniRow].Value = "Fecha";
            sheet.Range["B" + sHeaderIniRow].Value = "Invoice";
            sheet.Range["C" + sHeaderIniRow].Value = "Tipo Cobro";
            sheet.Range["D" + sHeaderIniRow].Value = "Estado";
            sheet.Range["E" + sHeaderIniRow].Value = "Con. de Embarque";
            sheet.Range["F" + sHeaderIniRow].Value = "Cheque";
            sheet.Range["G" + sHeaderIniRow].Value = "Banco";
            sheet.Range["H" + sHeaderIniRow].Value = "Monto Neto";

            SetTitleStyle(sheet.Range["A" + sHeaderIniRow]);
            SetTitleStyle(sheet.Range["B" + sHeaderIniRow]);
            SetTitleStyle(sheet.Range["C" + sHeaderIniRow]);
            SetTitleStyle(sheet.Range["D" + sHeaderIniRow]);
            SetTitleStyle(sheet.Range["E" + sHeaderIniRow]);
            SetTitleStyle(sheet.Range["F" + sHeaderIniRow]);
            SetTitleStyle(sheet.Range["G" + sHeaderIniRow]);
            SetTitleStyle(sheet.Range["H" + sHeaderIniRow]);
            SetTitleStyle(sheet.Range["A" + sHeaderIniRow + ":H" + sHeaderIniRow]);

            int originalPageSize = rdpPager.PageSize;
            int originalPageIndex = rdpPager.PageIndex;
            rdpPager.PageSize = 0;
            var row = dataIniRow;
            foreach (var j in (IEnumerable)rdpPager.PagedSource)
            {
                var r = j as FacturaAgenciamiento;
                if (r == null) continue;
                sheet.Range["A" + row].Value = r.Fecha.ToString();
                sheet.Range["B" + row].Value = r.Invoice;
                sheet.Range["C" + row].Value = r.TipoDeCobro;
                sheet.Range["D" + row].Value = r.Estado;
                sheet.Range["E" + row].Value = r.BL;
                sheet.Range["F" + row].Value = r.Cheque;
                sheet.Range["G" + row].Value = r.Banco;
                sheet.Range["H" + row].Value = r.MontoNeto.ToString();

                row++;
            }

            row--;

            SetDetailsStyle(sheet.Range["A" + dataIniRow + ":A" + row], ExcelHAlign.HAlignCenter, 15, "dd/mm/yyyy");
            SetDetailsStyle(sheet.Range["B" + dataIniRow + ":B" + row], ExcelHAlign.HAlignRight, 11, "0000000000");
            SetDetailsStyle(sheet.Range["C" + dataIniRow + ":C" + row], ExcelHAlign.HAlignCenter, 14, "@");
            SetDetailsStyle(sheet.Range["D" + dataIniRow + ":D" + row], ExcelHAlign.HAlignCenter, 12, "@");
            SetDetailsStyle(sheet.Range["E" + dataIniRow + ":E" + row], ExcelHAlign.HAlignRight, 20, "@");
            SetDetailsStyle(sheet.Range["F" + dataIniRow + ":F" + row], ExcelHAlign.HAlignRight, 25, "@");
            SetDetailsStyle(sheet.Range["G" + dataIniRow + ":G" + row], ExcelHAlign.HAlignCenter, 36, "@");
            SetDetailsStyle(sheet.Range["H" + dataIniRow + ":H" + row], ExcelHAlign.HAlignRight, 13, "0.000");


            sheet.Range["A" + dataIniRow + ":H" + row].BorderAround(ExcelLineStyle.Medium);

            sheet.Range["A" + dataIniRow + ":H" + row].Borders[ExcelBordersIndex.DiagonalUp].LineStyle =
                ExcelLineStyle.None;
            sheet.Range["A" + dataIniRow + ":H" + row].Borders[ExcelBordersIndex.DiagonalDown].LineStyle =
                ExcelLineStyle.None;

            rdpPager.PageSize = originalPageSize;
            rdpPager.PageIndex = originalPageIndex;



            workbook.SaveAs(code.Stream);
            workbook.Close();
            excelEngine.Dispose();
            code.Stream.Dispose();
            code = null;

            MessageBox.Show("El reporte fue generado satisfactoriamente", "Gits.NET", MessageBoxButton.OK);

        }


        private void SetTitleStyle(IRange range)
        {
            range.CellStyle.Font.Bold = true;
            range.CellStyle.HorizontalAlignment = ExcelHAlign.HAlignCenter;
            range.CellStyle.VerticalAlignment = ExcelVAlign.VAlignCenter;
            range.CellStyle.Color = Color.FromArgb(255, 183, 222, 232);

            range.BorderAround(ExcelLineStyle.Medium);
            range.Borders[ExcelBordersIndex.DiagonalUp].LineStyle = ExcelLineStyle.None;
            range.Borders[ExcelBordersIndex.DiagonalDown].LineStyle = ExcelLineStyle.None;
        }

        private void SetDetailsStyle(IRange range, ExcelHAlign hAlign, double size, string format)
        {
            range.ColumnWidth = size;
            range.NumberFormat = format;
            range.HorizontalAlignment = hAlign;
            range.VerticalAlignment = ExcelVAlign.VAlignCenter;
            range.BorderInside(ExcelLineStyle.Thin);
            range.BorderAround(ExcelLineStyle.Thin);

            range.Borders[ExcelBordersIndex.DiagonalUp].LineStyle = ExcelLineStyle.None;
            range.Borders[ExcelBordersIndex.DiagonalDown].LineStyle = ExcelLineStyle.None;
        }
    }
}
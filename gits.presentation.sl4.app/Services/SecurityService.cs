﻿#region

using System.Windows;
using gits.presentation.sl4.app.svcSeguridad;

#endregion

namespace gits.presentation.sl4.app.Services
{
    public class SecurityService : IApplicationService
    {
        private static SecurityService _current;

        private UserData _currentUser;
        private LoginServiceClient _svcLogin;

        #region Implementation of IApplicationService

        public void StartService(ApplicationServiceContext context)
        {
            _svcLogin = new LoginServiceClient();
            _svcLogin.LoginCompleted += OnLoginCompleted;
            _svcLogin.LogOutCompleted += OnLogOutCompleted;
            _current = this;
        }

        public void StopService()
        {
            _svcLogin = null;
            _current = null;
        }

        #endregion

        public static SecurityService Current
        {
            get { return _current; }
        }

        public UserData CurrentUser
        {
            get { return _currentUser; }
        }

        public LoginServiceClient LoginService
        {
            get { return _svcLogin; }
        }

        private void OnLogOutCompleted(object sender, LogOutCompletedEventArgs e)
        {
            _currentUser = null;
        }

        private void OnLoginCompleted(object sender, LoginCompletedEventArgs e)
        {
            if (e.Result.LoginError != null)
            {
                MessageBox.Show(e.Result.LoginError.ErrMsg);
                return;
            }
            _currentUser = e.Result;
            // MessageBox.Show("Bienvenido " + e.Result.UserRealName); 
        }
    }
}
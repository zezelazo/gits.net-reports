﻿#region

using System.Windows;
using gits.presentation.sl4.app.svcFacturacion;

#endregion

namespace gits.presentation.sl4.app.Services
{
    public class AppServices : IApplicationService
    {
        private static AppServices _current;
        private FacturacionServiceClient _servicioFacturacion;

        public static AppServices Current
        {
            get { return _current; }
        }

        public FacturacionServiceClient ServicioFacturacion
        {
            get
            {
                if (_servicioFacturacion == null) _servicioFacturacion = new FacturacionServiceClient();
                return _servicioFacturacion;
            }
        }

        #region IApplicationService Members

        public void StartService(ApplicationServiceContext context)
        {
            _current = this;
        }

        public void StopService()
        {
            _current = null;
        }

        #endregion
    }
}
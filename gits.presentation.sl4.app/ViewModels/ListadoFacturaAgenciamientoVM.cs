﻿#region

using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using gits.presentation.sl4.app.Controls;
using gits.presentation.sl4.app.Services;
using gits.presentation.sl4.app.svcFacturacion;

#endregion

namespace gits.presentation.sl4.app.ViewModels
{
    public class ListadoFacturaAgenciamientoVM : ViewModelBase
    {
        private string _bl;
        private string _cheque;
        private string _cliente;
        private DateTime? _desde;
        private bool _detalle;
        private DateTime? _hasta;
        private string _invoice;
        private bool _isBusy;
        private UIElement _resultView;
        private TipoDePagoAgenciamiento _tipoDePago;
        private ObservableCollection<TipoDePagoAgenciamiento> _tiposDePago;

        public ListadoFacturaAgenciamientoVM()
        {
            Desde = DateTime.Now;
            Hasta = DateTime.Now;
            int canExport = 0;
            TipoDePago = null;
            if (!IsInDesignMode)
            {
                AppServices.Current.ServicioFacturacion.GetTiposDePagoAgenciamientoCompleted +=
                    ServicioFacturacionGetTiposDePagoAgenciamientoCompleted;
                AppServices.Current.ServicioFacturacion.GetTiposDePagoAgenciamientoAsync(
                    SecurityService.Current.CurrentUser.UserEmpresaId);

                AppServices.Current.ServicioFacturacion.GetListadoFacturaSobreestadiaCompleted +=
                    ((s, e) => { IsBusy = false; });
                AppServices.Current.ServicioFacturacion.GetListadoFacturaAgenciamientoCompleted +=
                    ((s, e) => { IsBusy = false; });
            }
            Buscar = new RelayCommand(() =>
                                          {
                                              IsBusy = true;
                                              if (_detalle)
                                              {
                                                  var view = new ListadoFacturaAgenciamientoSobreestadia();
                                                  var viewModel = new ListadoFacturaAgenciamientoSobreestadiaVM();
                                                  view.DataContext = viewModel;
                                                  ResultView = view;
                                                  AppServices.Current.ServicioFacturacion.
                                                      GetListadoFacturaSobreestadiaAsync(
                                                          SecurityService.Current.CurrentUser.UserEmpresaId, Desde.Value,
                                                          Hasta.Value, Invoice, TipoDePago.Id, Cheque, Cliente);
                                              }
                                              else
                                              {
                                                  var view = new ListadoFacturaAgenciamiento();
                                                  var viewModel = new ListadoFacturaAgenciamientoVM();
                                                  view.DataContext = viewModel;
                                                  ResultView = view;
                                                  AppServices.Current.ServicioFacturacion.
                                                      GetListadoFacturaAgenciamientoAsync(
                                                          SecurityService.Current.CurrentUser.UserEmpresaId, Desde.Value,
                                                          Hasta.Value, Invoice, TipoDePago.Id, Cheque, Cliente);
                                              }
                                              canExport++;
                                              if (Exportar != null) Exportar.RaiseCanExecuteChanged();
                                              if (Exportar1 != null) Exportar1.RaiseCanExecuteChanged();
                                          },
                                      () =>
                                      Desde.HasValue && Hasta.HasValue && (Desde.Value <= Hasta.Value) &&
                                      TipoDePago != null);
            Exportar = new RelayCommand<Stream>((s) =>
            {
                if (_detalle)
                {
                    Messenger.Default.Send<ReportMsg>(new ReportMsg()
                    {
                        Report = "Sobreestadia",
                        Stream = s
                    });
                }
                else
                {
                    Messenger.Default.Send<ReportMsg>(new ReportMsg()
                    {
                        Report = "Agenciamiento",
                        Stream = s
                    });
                }
            },
                                        (s) => canExport > 0);

            Exportar1 = new RelayCommand(() => { }, () => canExport > 0); 
        }

        public RelayCommand Buscar { get; private set; }
        public RelayCommand<Stream> Exportar { get; private set; }
        public RelayCommand Exportar1 { get; private set; }

        public DateTime? Desde
        {
            get { return _desde; }
            set
            {
                if (_desde == value) return;
                _desde = value;
                RaisePropertyChanged("Desde");
                if (Buscar != null) Buscar.RaiseCanExecuteChanged();
            }
        }

        public DateTime? Hasta
        {
            get { return _hasta; }
            set
            {
                if (_hasta == value) return;
                _hasta = value;
                RaisePropertyChanged("Hasta");
                if (Buscar != null) Buscar.RaiseCanExecuteChanged();
            }
        }

        public bool Detalle
        {
            get { return _detalle; }
            set
            {
                if (_detalle == value) return;
                _detalle = value;
                RaisePropertyChanged("Detalle");
            }
        }

        public TipoDePagoAgenciamiento TipoDePago
        {
            get { return _tipoDePago; }
            set
            {
                if (_tipoDePago == value) return;
                _tipoDePago = value;
                RaisePropertyChanged("TipoDePago");
                if (Buscar != null) Buscar.RaiseCanExecuteChanged();
            }
        }

        public ObservableCollection<TipoDePagoAgenciamiento> TiposDePago
        {
            get { return _tiposDePago; }
            set
            {
                if (value == _tiposDePago) return;
                _tiposDePago = value;
                RaisePropertyChanged("TiposDePago");
            }
        }

        public UIElement ResultView
        {
            get { return _resultView; }
            set
            {
                if (value == _resultView) return;
                _resultView = value;
                RaisePropertyChanged("ResultView");
            }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (_isBusy == value) return;
                _isBusy = value;
                RaisePropertyChanged("IsBusy");
            }
        }

        public string Cheque
        {
            get { return _cheque; }
            set
            {
                if (_cheque == value) return;
                _cheque = value;
                RaisePropertyChanged("Cheque");
            }
        }

        public string Invoice
        {
            get { return _invoice; }
            set
            {
                if (_invoice == value) return;
                _invoice = value;
                RaisePropertyChanged("Invoice");
            }
        }

        public string BL
        {
            get { return _bl; }
            set
            {
                if (_bl == value) return;
                _bl = value;
                RaisePropertyChanged("BL");
            }
        }

        public string Cliente
        {
            get { return _cliente; }
            set
            {
                if (_cliente == value) return;
                _cliente = value;
                RaisePropertyChanged("Cliente");
            }
        }

        private void ServicioFacturacionGetTiposDePagoAgenciamientoCompleted(object sender,
                                                                             GetTiposDePagoAgenciamientoCompletedEventArgs
                                                                                 e)
        {
            TiposDePago = e.Result;
        }
    }
}
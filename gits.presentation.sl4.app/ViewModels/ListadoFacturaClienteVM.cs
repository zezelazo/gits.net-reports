﻿#region

using System;
using System.IO;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using gits.presentation.sl4.app.Controls;
using gits.presentation.sl4.app.Services;

#endregion

namespace gits.presentation.sl4.app.ViewModels
{
    public class ListadoFacturaClienteVM : ViewModelBase
    {
        private DateTime? _desde;
        private bool _detalle;
        private bool _soloDetalle;
        private DateTime? _hasta;
        private bool _isBusy;
        private UIElement _resultView;

        public ListadoFacturaClienteVM()
        {
            Desde = DateTime.Now;
            Hasta = DateTime.Now;
            int canExport = 0;
            if (!IsInDesignMode)
            {
                AppServices.Current.ServicioFacturacion.GetListadoFacturaConsolidadoCompleted +=
                    ((s, e) => { IsBusy = false; });
                AppServices.Current.ServicioFacturacion.GetListadoFacturaDetalladoCompleted +=
                    ((s, e) => { IsBusy = false; });
                AppServices.Current.ServicioFacturacion.GetListadoFacturaDetalladoSoloDetalleCompleted +=
                    ((s, e) => { IsBusy = false; });
            }
            Buscar = new RelayCommand(() =>
                                          {
                                              IsBusy = true;
                                              if (_detalle)
                                              {
                                                  var view = new ListadoFacturaClienteDetallado();
                                                  var viewModel = new ListadoFacturaClienteDetalladoVM();
                                                  view.DataContext = viewModel;
                                                  ResultView = view;
                                                  if (_soloDetalle)
                                                  {
                                                      AppServices.Current.ServicioFacturacion.
                                                          GetListadoFacturaDetalladoSoloDetalleAsync(
                                                                SecurityService.Current.CurrentUser.UserEmpresaId, Desde.Value,
                                                                Hasta.Value);
                                                  }
                                                  else
                                                  {
                                                      AppServices.Current.ServicioFacturacion.
                                                       GetListadoFacturaDetalladoAsync(
                                                           SecurityService.Current.CurrentUser.UserEmpresaId, Desde.Value,
                                                           Hasta.Value);
                                                  }

                                              }
                                              else
                                              {
                                                  var view = new ListadoFacturaCliente();
                                                  var viewModel = new ListadoFacturaClienteConsolidadoVM();
                                                  view.DataContext = viewModel;
                                                  ResultView = view;
                                                  AppServices.Current.ServicioFacturacion.
                                                      GetListadoFacturaConsolidadoAsync(
                                                          SecurityService.Current.CurrentUser.UserEmpresaId, Desde.Value,
                                                          Hasta.Value);
                                              }
                                              canExport++;
                                              if (Exportar != null) Exportar.RaiseCanExecuteChanged();
                                              if (Exportar1 != null) Exportar1.RaiseCanExecuteChanged();
                                          },
                                      () => Desde.HasValue && Hasta.HasValue && (Desde.Value <= Hasta.Value));

            Exportar = new RelayCommand<Stream>((s) =>
                                            {
                                                if (_detalle)
                                                {
                                                    Messenger.Default.Send<ReportMsg>(new ReportMsg()
                                                                                          {
                                                                                              Report = "Detallado",
                                                                                              Stream=s
                                                                                          });
                                                }
                                                else
                                                {
                                                    Messenger.Default.Send<ReportMsg>(new ReportMsg()
                                                                                          {
                                                                                              Report = "Cliente",
                                                                                              Stream = s
                                                                                          });
                                                }
                                            },
                                        (s) => canExport > 0);

            Exportar1 = new RelayCommand(() => { }, () => canExport > 0); 

            Messenger.Default.Register<String>(this, (m) =>
                                                         {
                                                             if (m == "IsBusy") IsBusy = true;
                                                             if (m == "IsNotBusy") IsBusy = false;
                                                         });
        }

        public RelayCommand Buscar { get; private set; }
        public RelayCommand<Stream> Exportar { get; private set; }

        public RelayCommand Exportar1 { get; private set; }

        public DateTime? Desde
        {
            get { return _desde; }
            set
            {
                if (_desde == value) return;
                _desde = value;
                RaisePropertyChanged("Desde");
                if (Buscar != null) Buscar.RaiseCanExecuteChanged();
            }
        }

        public DateTime? Hasta
        {
            get { return _hasta; }
            set
            {
                if (_hasta == value) return;
                _hasta = value;
                RaisePropertyChanged("Hasta");
                if (Buscar != null) Buscar.RaiseCanExecuteChanged();
            }
        }

        public bool Detalle
        {
            get { return _detalle; }
            set
            {
                if (_detalle == value) return;
                _detalle = value;
                RaisePropertyChanged("Detalle");
            }
        }


        public bool SoloDetalle
        {
            get { return _soloDetalle; }
            set
            {
                if (_soloDetalle == value) return;
                _soloDetalle = value;
                RaisePropertyChanged("SoloDetalle");
            }
        }
        public UIElement ResultView
        {
            get { return _resultView; }
            set
            {
                if (value == _resultView) return;
                _resultView = value;
                RaisePropertyChanged("ResultView");
            }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (_isBusy == value) return;
                _isBusy = value;
                RaisePropertyChanged("IsBusy");
            }
        }
    }
}
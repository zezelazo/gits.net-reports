﻿#region

using System.Windows;
using System.Windows.Controls;
using gits.presentation.sl4.app.Services;
using gits.presentation.sl4.app.svcSeguridad;

#endregion

namespace gits.presentation.sl4.app
{
    public partial class MainPage : UserControl
    {
        public MainPage()
        {
            InitializeComponent();
            SecurityService.Current.LoginService.LoginCompleted += LoginService_LoginCompleted;
            SecurityService.Current.LoginService.LogOutCompleted += LoginService_LogOutCompleted;
            MainLayer.Visibility = Visibility.Collapsed;
            LoginLayer.Visibility = Visibility.Visible;
        }

        private void LoginService_LogOutCompleted(object sender, LogOutCompletedEventArgs e)
        {
            MainLayer.Visibility = Visibility.Collapsed;
            LoginLayer.Visibility = Visibility.Visible;
        }

        private void LoginService_LoginCompleted(object sender, LoginCompletedEventArgs e)
        {
            if (e.Result.LoginError == null)
            {
                MainLayer.Visibility = Visibility.Visible;
                LoginLayer.Visibility = Visibility.Collapsed;
            }
        }
    }
}
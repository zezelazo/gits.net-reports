﻿using System;
using System.Data;
using gits.domain.core;
using gits.infraestructure.data.core.context;

namespace gits.infraestructure.data.core.repositories
{
    public abstract class RepositoryBase : IDisposable
    {
        public GenericContext Context { get; set; }

        protected RepositoryBase(IContext prmContext)
        {
            Context = prmContext as GenericContext;
        }

        #region Implementation of IDisposable

        public void Dispose()
        {

            if (Context != null)
            { 
                Context.Dispose();
            }
        }

        #endregion

    }
}
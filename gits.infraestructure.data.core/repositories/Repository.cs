﻿using System.Data;
using System.Data.Objects;
using gits.domain.core;
using gits.domain.core.entities.ef4;
using gits.infraestructure.data.core.context; 

namespace gits.infraestructure.data.core.repositories
{
    public class GenericRepository<T> : ReadOnlyRepository<T>, IRepository<T> where T : TrackeableEntity 
    {
        public GenericRepository(IContext prmContext) : base(prmContext)
        {
        }

        public void Add(T entity)
        {
            Context.CreateSet<T>().AddObject(entity);
        }

        public void Remove(T entity)
        {
            Context.CreateSet<T>().DeleteObject(entity);
        }

        public void Update(T current)
        {
            ObjectStateEntry entry = null;

            if (Context.ObjectStateManager.TryGetObjectStateEntry(current, out entry))
            {
                Context.ObjectStateManager.ChangeObjectState(current, EntityState.Modified);
            }
            else
            {
                Context.CreateSet<T>().Attach(current);
                Context.ObjectStateManager.ChangeObjectState(current, EntityState.Modified);
            }
        }

        public void Attach(T current)
        {
            Context.CreateSet<T>().Attach(current);
        }

        public void SubmitChanges()
        {
            Context.SaveChanges();
        }
    }
}
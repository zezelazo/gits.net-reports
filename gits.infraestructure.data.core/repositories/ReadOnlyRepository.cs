﻿using System;
using System.Linq;
using System.Linq.Expressions;
using gits.domain.core;
using gits.domain.core.entities.ef4;
using gits.infraestructure.data.core.context; 

namespace gits.infraestructure.data.core.repositories
{
    public class ReadOnlyRepository<T> : RepositoryBase, IReadOnlyRepository<T> where T : class,IObjectWithChangeTracker
    {
        public ReadOnlyRepository(IContext prmContext)
            : base(prmContext)
        {
        }

        public virtual IQueryable<T> Query()
        { 
            return Context.CreateSet<T>();
        }

        public virtual IQueryable<T> Find(Expression<Func<T, bool>> expression)
        {
            return Context.CreateSet<T>().Where(expression).AsQueryable();
        }

    }
}

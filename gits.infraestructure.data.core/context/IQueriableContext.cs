﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using gits.domain.core;
using gits.domain.core.entities.ef4; 

namespace gits.infraestructure.data.core.context
{
    public interface IQueriableContext : IContext
    {
        IObjectSet<TEntity> CreateSet<TEntity>() where TEntity : class, IObjectWithChangeTracker;
        IList<TResult> GetFunctionResult<TResult>(string FunctionName, params ObjectParameter[] parameters);
    }


     
}

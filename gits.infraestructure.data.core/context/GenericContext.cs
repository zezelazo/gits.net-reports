﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Linq;
using gits.domain.core.entities.ef4;

namespace gits.infraestructure.data.core.context
{
    public class GenericContext : ObjectContext, IQueriableContext
    {
        #region Constructors
         

        public  string ConnectionString { get; set; } //"name=GitsSecurity";
        public  string ContainerName { get; set; } // "GitsSecurity";


        public GenericContext(string prmconnectionString)
            : base(prmconnectionString)
        {
            Initialize();
        }

        public GenericContext(EntityConnection prmConnection)
            : base(prmConnection)
        {
            Initialize();
        }


        public GenericContext(string prmconnectionString, string prmContainerName)
            : base(prmconnectionString, prmContainerName)
        {
            Initialize();
        }

        public GenericContext(EntityConnection prmConnection, string prmContainerName)
            : base(prmConnection, prmContainerName)
        {
            Initialize();
        }

        protected internal void Initialize()
        { 
            ContextOptions.ProxyCreationEnabled = false;
            //ContextOptions.LazyLoadingEnabled = false;
            ObjectMaterialized += HandleObjectMaterialized;
        }

        private void HandleObjectMaterialized(object sender, ObjectMaterializedEventArgs e)
        {
            var entity = e.Entity as IObjectWithChangeTracker;
            if (entity != null)
            {
                bool changeTrackingEnabled = entity.ChangeTracker.ChangeTrackingEnabled;
                try
                {
                    entity.MarkAsUnchanged();
                }
                finally
                {
                    entity.ChangeTracker.ChangeTrackingEnabled = changeTrackingEnabled;
                }
                this.StoreReferenceKeyValues(entity);
            }
        }

        #endregion

        
        public DbTransaction GetTransaction()
        {
            if (Connection.State != ConnectionState.Open)
            {
                    Connection.Open();
            }
            return Connection.BeginTransaction();
             
        }

        public IObjectSet<TEntity> CreateSet<TEntity>() where TEntity : class, IObjectWithChangeTracker
        {
            ObjectSet<TEntity> objS;objS = CreateObjectSet<TEntity>();
            objS.EnablePlanCaching = true;
            return objS; 
        }

        public IList<TResult> GetFunctionResult<TResult>(string functionName, params ObjectParameter[] parameters)
        {
            var rpta = ExecuteFunction<TResult>(functionName, parameters);
            if (rpta == null) return null;
            return rpta.ToList();
        }
    }

}
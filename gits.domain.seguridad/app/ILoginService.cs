﻿using System;
using System.Collections.Generic;
using gits.domain.seguridad.entities;

namespace gits.domain.seguridad.app
{
    public interface ILoginService : IDisposable 
    {
        IList<Empresa> GetEmpresas();
        
        bool CheckIfUserExist(string prmUserName);
        bool CheckIfUserExistInEmpresa(string prmUserName,string prmEmpresaCode);
        bool CheckIfIsUserPassword(string prmUserName, string prmUserPassword);
        
        Usuario GetUserData(string prmUserCode);
        Grupo GetUserGroup(string prmUserCode);
        Persona GetUserPersonalInfo(string prmUserCode);
        IList<Empresa> GetUserEmpresas(string prmUserCode);

        Usuario GetFullUserData(string prmUserCode);
    }
}

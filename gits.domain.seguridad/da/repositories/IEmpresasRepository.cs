﻿using gits.domain.core;
using gits.domain.seguridad.entities;

namespace gits.domain.seguridad.da.repositories
{
    public interface IEmpresasRepository : IReadOnlyRepository<Empresa>
    {
    }
}
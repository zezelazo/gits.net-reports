﻿using System.Runtime.Serialization;

namespace gits.distributedservices.facturacion.dto
{ 
    public class FacturaConsolidada
    {
        
        public string FormaDePago { get; set; }
        
        public string NroDocumentoDePago { get; set; }
        
        public string Banco { get; set; }
        
        public string FechaPago { get; set; }
        
        public string Moneda { get; set; }//US
        
        public string RucAgente { get; set; }
        
        public string Agente { get; set; }
        
        public decimal? TipoDeCambio { get; set; }
        
        public decimal? Retencion { get; set; }
        
        public decimal? Monto { get; set; }
        
        public string Comprobante { get; set; }
        
        public string Serie { get; set; }
        
        public string Numero { get; set; }
        
        public string FechaRegistro { get; set; }
        
        public string FacturarA { get; set; }
        
        public decimal? FacturaMontoNeto { get; set; }
        
        public decimal? FacturaMontoImpuesto { get; set; }
        
        public decimal? FacturaMontoBruto { get; set; }
        
        public decimal? FacturaMontoNetoDolares { get; set; }
        
        public decimal? FacturaMontoImpuestoDolares { get; set; }
        
        public decimal? FacturaMontoBrutoDolares { get; set; }
        
        public string Ruc { get; set; }
        
        public string BL_M { get; set; }
        
        public string BL { get; set; }
        
        public string Nave { get; set; }
        
        public string Viaje { get; set; }
        
        public string Empresa { get; set; }
        
        public string SimboloMoneda { get; set; }
    }
}
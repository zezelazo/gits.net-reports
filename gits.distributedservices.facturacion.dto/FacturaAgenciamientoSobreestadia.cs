﻿using System;
using System.Runtime.Serialization;

namespace gits.distributedservices.facturacion.dto
{ 
    public  class FacturaAgenciamientoSobreestadia
    {

        
        
        public string Invoice { get; set; } //FACAGE_C_NUMERO
        
        public string Cheque { get; set; } //FACAGE_C_NUMERO_CHEQUE 
        
        public string TipoDeCobro { get; set; }//TIPAAG_C_DESCRIPCION
        
        public DateTime? Fecha { get; set; } //FACAGE_D_FECHA 
        
        public string BL { get; set; }//FACAGE_C_DOCUMENTO_ORIGEN 
        
        public string Banco { get; set; }//BANCO_C_DESCRIPCION 
        
        public string Estado { get; set; } //FACAGE_C_SITUACION 
        
        public string Ruc { get; set; }//CLIENT_C_CODIGO 
        
        public string Cliente { get; set; }//CLIENT_C_RAZON_SOCIAL 
        
        public decimal? Total{ get; set; }//FACAGE_N_TOTAL 
        
        public decimal? Retencion { get; set; }//FACAGE_N_RETENCION 
        
        public decimal? Neto { get; set; }//FACAGE_N_NETO 
        
        public string DesdeHasta { get; set; }//DESDEHASTA
        
        public decimal? TotalCheque { get; set; }//FACAGE_N_TOTAL_CHEQUE
    }
}
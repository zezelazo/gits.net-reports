﻿using System;
using System.Runtime.Serialization;

namespace gits.distributedservices.facturacion.dto
{ 
    public class FacturaAgenciamiento
    {
        
        public string Empresa { get; set; }
        
        public string Invoice { get; set; } //FACAGE_C_NUMERO
        
        public string Cheque { get; set; } //FACAGE_C_NUMERO_CHEQUE 
        
        public string TipoDeCobro { get; set; }//TIPAAG_C_DESCRIPCION
        
        public DateTime? Fecha { get; set; } //FACAGE_D_FECHA 
        
        public string BL { get; set; }//FACAGE_C_DOCUMENTO_ORIGEN 
        
        public string Banco { get; set; }//BANCO_C_DESCRIPCION 
        
        public decimal MontoNeto { get; set; } //
        
        public string Estado { get; set; } //FACAGE_C_SITUACION 
    }
}

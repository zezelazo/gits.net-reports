﻿using System.Runtime.Serialization;

namespace gits.distributedservices.facturacion.dto
{
    [DataContract]
    public class TipoDePagoAgenciamiento
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
    }
}
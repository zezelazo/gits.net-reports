﻿using System.Collections.Generic;
using System.ServiceModel;
using gits.distributedservices.seguridad.dto;

namespace gits.distributedservices.seguridad.contracts
{
    [ServiceContract]//(SessionMode = SessionMode.Required)]
    public interface ILoginService
    {
        [OperationContract]// (IsInitiating = false, IsTerminating = false)] 
        UserData Login(UserCredentials prmCredentials);

        [OperationContract]//(IsInitiating = false,IsTerminating = true)]
        int LogOut(string prmSession);

        [OperationContract]//(IsInitiating = false, IsTerminating = false)] 
        bool IsValidSession(string prmSessionId);

        [OperationContract]//(IsInitiating = true, IsTerminating = false)] 
        IEnumerable<Empresa> ListaEmpresas();
    }
}

﻿using System.Runtime.Serialization;

namespace gits.distributedservices.seguridad.dto
{
    [DataContract]
    public class UserCredentials
    {
        [DataMember]
        public string UserCompany { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string UserPassword { get; set; }
        [DataMember]
        public string ClientIp { get; set; }
        [DataMember]
        public string ClientName { get; set; }
        [DataMember]
        public ClientsTypesEnum ClientType { get; set; }
    }
}

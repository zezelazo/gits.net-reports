﻿namespace gits.distributedservices.seguridad.dto
{
    public enum ClientsTypesEnum
    {
        Desktop,
        Web,
        Silverlight,
        WMobile
    }
}
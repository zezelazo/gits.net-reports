﻿using System;
using System.Runtime.Serialization;

namespace gits.distributedservices.seguridad.dto
{
    [DataContract]
    public class SesionInfo
    {
        [DataMember]
        public Guid SessionCode { get; set; }
        [DataMember]
        public DateTime StartTime { get; set; }
        [DataMember]
        public DateTime? EndTime { get; set; }
        [DataMember]
        public string UserCode { get; set; }
        [DataMember]
        public ClientsTypesEnum ClientType { get; set; }
        [DataMember]
        public string ClientIp { get; set; }
        [DataMember]
        public string ClientName { get; set; }
    }
}
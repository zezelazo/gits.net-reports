﻿using System.Runtime.Serialization;

namespace gits.distributedservices.seguridad.dto
{
    [DataContract]
    public class LoginError
    {
        [DataMember]
        public int ErrCode { get; set; }
        [DataMember]
        public string ErrMsg { get; set; }
    }
}

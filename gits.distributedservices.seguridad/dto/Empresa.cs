﻿using System.Runtime.Serialization;

namespace gits.distributedservices.seguridad.dto
{
    [DataContract]
    public class Empresa
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Descripcion { get; set; }
    }
}
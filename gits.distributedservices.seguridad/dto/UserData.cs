﻿using System;
using System.Runtime.Serialization;

namespace gits.distributedservices.seguridad.dto
{
    [DataContract]
    public class UserData
    {
        [DataMember]
        public string UserRealName { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string UserEmpresa { get; set; }
        [DataMember]
        public string UserEmpresaId { get; set; }
        [DataMember]
        public string UserCode { get; set; }
        [DataMember]
        public Guid SessionCode { get; set; }
        [DataMember]
        public LoginError LoginError { get; set; }

    }
}
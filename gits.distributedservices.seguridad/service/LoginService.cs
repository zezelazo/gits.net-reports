﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using gits.distributedservices.seguridad.contracts;
using gits.distributedservices.seguridad.dto;
using gits.domain.seguridad.da.context;
using gits.domain.seguridad.da.repositories;
using gits.infraestructure.data.security.context;
using gits.infraestructure.data.security.repositories;

namespace gits.distributedservices.seguridad.service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class LoginService : ILoginService
    {
        private List<string> _sessions;

        public LoginService()
        {
            _sessions = new List<string>();
        }

        public UserData Login(UserCredentials prmCredentials)
        {
            prmCredentials.ClientIp = System.Web.HttpContext.Current.Request.UserHostAddress ?? string.Empty;
            prmCredentials.ClientName = System.Web.HttpContext.Current.Request.UserHostName ?? string.Empty; 
            var rpta = new UserData();
            
            using (var segCtx = new seguridadContext())
            {
                using (var srvLogin = new application.seguridad.login.LoginService(new UsuariosRepository(segCtx),
                                                                                     new EmpresasRepository(segCtx),
                                                                                     new GruposRepository(segCtx)))
                {
                    if (!(srvLogin.CheckIfUserExist(prmCredentials.UserName)))
                    {
                        rpta.LoginError = new LoginError
                                              {
                                                  ErrCode = (int)GitsLoginError.BadName,
                                                  ErrMsg = "El usuario especificado no existe"
                                              };
                        return rpta;
                    }

                    if (!(srvLogin.CheckIfUserExistInEmpresa(prmCredentials.UserName, prmCredentials.UserCompany)))
                    {
                        rpta.LoginError = new LoginError
                        {
                            ErrCode = (int)GitsLoginError.NoInCompany,
                            ErrMsg = "El usuario especificado no esta asociado a la compania seleccionada"
                        };
                        return rpta;
                    }

                    if (!(srvLogin.CheckIfIsUserPassword(prmCredentials.UserName, prmCredentials.UserPassword)))
                    {
                        rpta.LoginError = new LoginError
                        {
                            ErrCode = (int)GitsLoginError.BadPassword,
                            ErrMsg = "La contrasena no coinside"
                        };
                        return rpta;
                    }

                    if (rpta.LoginError == null)
                    {
                        var usrData = srvLogin.GetFullUserData(prmCredentials.UserName);
                        rpta.UserName = prmCredentials.UserName;
                        rpta.UserRealName = usrData.Persona.NombreCompleto;
                        rpta.UserCode = usrData.Id;
                        rpta.UserEmpresa = prmCredentials.UserCompany;
                        rpta.SessionCode = new Guid();
                        rpta.UserEmpresaId =
                            srvLogin.GetUserEmpresas(prmCredentials.UserName).Where(
                                e => e.Id == prmCredentials.UserCompany).First().Id;
                       
                        _sessions.Add(rpta.SessionCode.ToString());
                    }


                    return rpta;
                }
            }
        }

        public int LogOut(string prmSessionId)
        {
            if (_sessions.Contains(prmSessionId))
            {
                _sessions.Remove(prmSessionId);
                return 1;
            }
            return -1;
        }

        public IEnumerable<Empresa> ListaEmpresas()
        {
            var segCtx = new seguridadContext();


            using (var srvLogin = new application.seguridad.login.LoginService(new UsuariosRepository(segCtx),
                                                                      new EmpresasRepository(segCtx),
                                                                      new GruposRepository(segCtx)))
            {
                var rpta = srvLogin.GetEmpresas().Select(e => new Empresa()
                {
                    Id = e.Id,
                    Descripcion = e.Descripcion
                });
                return rpta;
            }
        }

        public bool IsValidSession(string prmSessionId)
        {
            return _sessions.Contains(prmSessionId);
        }
    }
}

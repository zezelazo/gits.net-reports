﻿using System;
using System.Collections.Generic;
using System.Linq;
using gits.domain.facturacion.app;
using gits.domain.facturacion.da.repositories;
using gits.domain.facturacion.entities;

namespace gits.application.facturacion.reports
{
    public class ReportesFacturacionService : IReportesFacturacionService
    {
        private IReportesRepository _mainRepo;
        private ITipoPagoAgenciamientoRepository _tpaRepo;
        public ReportesFacturacionService(IReportesRepository prmRepo, ITipoPagoAgenciamientoRepository prmTpaRepo)
        {
            _mainRepo = prmRepo;
            _tpaRepo = prmTpaRepo;
        }

        public IList<ListadoFacturaDetallado> GetListadoFacturaDetalladoSoloDetalle(string empresa, DateTime desde, DateTime hasta)
        {
            if (String.IsNullOrWhiteSpace(empresa)) return null; //TODO: RETURN A PROPER ERROR MESSAGE
            return _mainRepo.GetListadoFacturaDetalladoSoloDetalles(empresa, FormatDate(desde), FormatDate(hasta));
        }

        public IList<ListadoFacturaDetallado> GetListadoFacturaDetallado(string empresa, DateTime desde, DateTime hasta)
        {
            if (String.IsNullOrWhiteSpace(empresa)) return null; //TODO: RETURN A PROPER ERROR MESSAGE
            return _mainRepo.GetListadoFacturaDetallado(empresa, FormatDate(desde), FormatDate(hasta));
        }

        public IList<ListadoFacturaConsolidado> GetListadoFacturaConsolidado(string empresa, DateTime desde, DateTime hasta)
        {
            if (String.IsNullOrWhiteSpace(empresa)) return null; //TODO: RETURN A PROPER ERROR MESSAGE
            return _mainRepo.GetListadoFacturaConsolidado(empresa, FormatDate(desde), FormatDate(hasta));
        }

        public IList<FacturaAgenciamiento> GetListadoFacturaAgenciamiento(string empresa, DateTime desde, DateTime hasta, string invoice, string tipocobro, string cheque, string cliente)
        {
            if (String.IsNullOrWhiteSpace(empresa)) return null; //TODO: RETURN A PROPER ERROR MESSAGE

            if (String.IsNullOrEmpty(invoice)) invoice = string.Empty;
            if (String.IsNullOrEmpty(tipocobro)) tipocobro = string.Empty;
            if (String.IsNullOrEmpty(cheque)) cheque = string.Empty;
            if (String.IsNullOrEmpty(cliente)) cliente = string.Empty;

            return _mainRepo.GetListadoFacturaAgenciamiento(empresa, FormatDate(desde), FormatDate(hasta), invoice, tipocobro, cheque, cliente);
        }

        public IList<FacturaAgenciamientoSobreestadia> GetListadoFacturaSobreestadia(string empresa, DateTime desde, DateTime hasta, string invoice, string tipocobro, string cheque, string cliente)
        {
            if (String.IsNullOrWhiteSpace(empresa)) return null; //TODO: RETURN A PROPER ERROR MESSAGE

            if (String.IsNullOrEmpty(invoice)) invoice = string.Empty;
            if (String.IsNullOrEmpty(tipocobro)) tipocobro = string.Empty;
            if (String.IsNullOrEmpty(cheque)) cheque = string.Empty;
            if (String.IsNullOrEmpty(cliente)) cliente = string.Empty;

            return _mainRepo.GetListadoFacturaSobreestadia(empresa, FormatDate(desde), FormatDate(hasta), invoice, tipocobro, cheque, cliente);
        }

        public IList<TipoPagoAgenciamiento> GetTipoPagoAgenciamiento(string empresa)
        {
            return _tpaRepo.Find(t => t.EmpresaId == empresa).OrderBy(t => t.Descripcion).ToList();
        }

        private string FormatDate(DateTime prmDate)
        {
            return string.Format("{0:yyyyMMdd}", prmDate);
        }

        #region Implementation of IDisposable

        public void Dispose()
        {
            _mainRepo.Dispose();
            _tpaRepo.Dispose();
        }

        #endregion
    }
}

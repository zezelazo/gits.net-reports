﻿using System;

namespace gits.domain.core.entities
{
    public abstract class AuditableEntity : NotificableObject
    {
        private string _creator;
        public virtual string Creator
        {
            get
            {
                return _creator;
            }
            set
            {
                if (_creator != value)
                {
                    _creator = value;
                    RaisePropertyChanged("Creator");
                }
            }
        }

        private string _modificator;
        public virtual string Modificator
        {
            get
            {
                return _modificator;
            }
            set
            {
                if (_modificator != value)
                {
                    _modificator = value;
                    RaisePropertyChanged("Modificator");
                }
            }
        } 
         
        private DateTime _creationDate;
        public virtual DateTime CreationDate
        {
            get
            {
                return _creationDate;
            }
            set
            {
                if (_creationDate != value)
                {
                    _creationDate = value;
                    RaisePropertyChanged("CreationDate");
                }
            }
        }  

        private DateTime _modificationDate;
        public virtual DateTime ModificationDate
        {
            get
            {
                return _modificationDate;
            }
            set
            {
                if (_modificationDate != value)
                {
                    _modificationDate = value;
                    RaisePropertyChanged("ModificationDate");
                }
            }
        } 
    }
}